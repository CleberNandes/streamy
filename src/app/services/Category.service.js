// CategoryService
const categories = [
  {
    label: 'Saúde',
    color: 'text-warning',
    background: 'warning',
    icon: 'fa-heartbeat'
  },
  {
    label: 'Nutrição',
    color: 'text-success',
    background: 'seccess',
    icon: 'fa-cutlery'
  },
  {
    label: 'Performance',
    color: 'text-info',
    background: 'info',
    icon: 'fa-500px'
  },
  {
    label: 'Notícias',
    color: 'text-danger',
    background: 'danger',
    icon: 'fa-free-code-camp'
  },
  {
    label: 'Medicina Esportiva',
    color: 'text-pink',
    background: 'pink',
    icon: 'fa-user-md'
  },
  {
    label: 'Treino',
    color: 'text-purple',
    background: 'purple',
    icon: 'fa-child'
  }

];


const getCategory = (name) => {
  return categories.find(item => item.label === name);
}

const all = () => {
  return categories.map(item => item.label)
}




export default {
  getCategory, all
}