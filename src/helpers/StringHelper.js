import React from 'react'


const passSentenceToSlug = (text) => {
  text = takeOffSpecialChar(text);
  return text ? text.toLowerCase().replace(/ /g,"-") : new Date().getTime();
};

const handleTextareaSplitPAndTitle = (text) => {
  return text.split('\n\n')
    .map((item, i) => {
      const itens = item.split('\n');
      return itens.map((sub, j) => {
        if (itens.length === 1) {
          if (sub.length < 60) {
            return <h5 key={i + j}>{sub}</h5>
          }
          return <p key={i + j}>{sub}</p>
        } 
        return <p className="list-item-p" key={i + j}>{sub}</p>
      })
    });
}

const takeOffSpecialChar = (text) => {
  return text ? text
    .replace('.', '')
    .replace(',', '')
    .replace(':', '')
    .replace(';', '')
    .replace('+', '')
    .replace('=', '')
    .replace('?', '')
    .replace('!', '')
    .replace('#', '')
    .replace('$', '')
    .replace('*', '')
    .replace('&', '')
    .replace('@', '')
    .replace('"', '')
    .replace('\'', '')
    .replace('{', '')
    .replace('}', '')
    .replace('(', '')
    .replace(')', '')
    .replace('[', '')
    .replace(']', '') : null;
    // .replace(/.|#|$|[|]/, '')
}

const capitalize = (s) => {  
  if (typeof s !== 'string') return ''
  const text = s.toLowerCase();
  return text.charAt(0).toUpperCase() + text.slice(1);
}

const truncateText = (text, length) => {
  return text.substring(0, length) + 
    (text.length > length ? '...' : '');
}


export default {
  passSentenceToSlug,
  handleTextareaSplitPAndTitle,
  capitalize,
  truncateText
};