/** 
 * @author Cleber Antônio Fernandes
 * @email cleber_sim@outlook.com
 * @howToUse useInput(false, () => fieldName.set(!fieldName.val))
 * @props 
 * @bind object useInput
 * @label string 
 * @required boolean
 * @errorMsg string
*/

import React from 'react';
import { FormGroup, Label, Input } from 'reactstrap';

function InputCheckbox({field, label}) {

  const handleClick = (e) => {
    console.log(e.target.value);
    field.set(!field.value);
  }

  return (
    <FormGroup check>
      <Label check>
        <Input
          type="checkbox"
          // checked={props.value}
          {...field.bind}/>
        
        {label}
        <span className="form-check-sign" />
      </Label>

      { field.error ? 
        <small className="invalid-feedback">
          {field.error}
        </small> 
      : null }

    </FormGroup>
  )
}

export default InputCheckbox;