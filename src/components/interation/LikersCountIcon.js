import React, { useState } from 'react'
import { UncontrolledTooltip } from 'reactstrap';
import FirebaseService from '../../app/firebase/Firebase.service';
import { ModalLoginConsumer } from '../../app/pages/sign/ModalLoginContext';
import GeneralHelpers from '../../helpers/GeneralHelpers';

function LikersCountIcon({likers, user, slug, _setLogin}) {

  const [_likers, _setLikers] = useState(likers);
  let [_length, setLength] = useState( GeneralHelpers.arrayLength(_likers) );
  const [_like, _setLike] = useState(_likers ? !!_likers[user.uid] : false);

  const addLiker = () => {
    _setLike(true);
    FirebaseService.getItemFromDatabase('posts', slug + '/likers', _setLikers);
    FirebaseService.post(slug).child('likers/' + user.uid)
      .set(user.email).then(() => setLength(GeneralHelpers.arrayLength(_likers) + 1));
  }

  const removeLiker = () => {
    FirebaseService.deleteItem('posts', slug + '/likers/' + user.uid);
    FirebaseService.getItemFromDatabase('posts', slug + '/likers', (likers) => {
      _setLikers(likers);
      setLength(GeneralHelpers.arrayLength(_likers) - 1);
      _setLike(false);
    });
  }

  const likeClick = () => {
    if (user) {
      if (!_like) {
        addLiker()
      } else {
        removeLiker();
      }
    } else {
      _setLogin(true)
    }
  }

  return (
    <div className="amount-item">
      <div id="likes"
        className="item-icon-container"
        onClick={likeClick}>
        <i className={"fa fa-heart "+(_like ? 'selected':'')}></i>
      </div>
      <span>{_length ? _length : 0}</span>

      <UncontrolledTooltip placement="top" target="likes">
        {_like ? 'Desmarcar curtição' : 'Curtir artigo'}
      </UncontrolledTooltip>
    </div>
  )
}

export default ModalLoginConsumer(LikersCountIcon)