import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Badge, Button, Card, Container, Row, Col } from "reactstrap";

// reactstrap components
import FirebaseService from '../../firebase/Firebase.service';
import AuthorService from "../../services/Author.service";
import StringHelper from "../../../helpers/StringHelper";
import DateHelper from "../../../helpers/DateHelper";
import GeneralHelpers from "../../../helpers/GeneralHelpers";
import DangerNavbar from "../../../components/Navbars/DangerNavbar";
import navbar from "../../../data/NavbarData";
import GradientHeader from "../../../components/Headers/GradientHeader";
import InterationAmount from "../../../components/interation/InterationAmount";
import PostAuthorInfo from "./PostAuthorInfo";
import CommentContainer from "../../../components/comment/CommentContainer";
import RelatedPosts from "../../../components/sections/RelatedPosts";
import FooterGray from "../../../components/Footers/FooterGray";
import { AuthenticationConsumer } from "../auth/AuthenticationContext";




const mock = {
  date: {
    formatted: '2020-02-24',
    pretty: '24 de fevereiro de 2020'
  },
  image: require("../../../assets/img/sections/daniel-olahs.jpg"),
  imageAlt: "...",
  title: 'LinkedIn’s new desktop app arrives',
  slug: 'linkedIn-new-desktop-app-arrives,',
  description: `
    You won’t find many concepts that are very useful or
    important if you insist on having a worldview that’s void
    of controversy, invulnerable to criticism, and incapable
    of making others feel confused.\n\n

    Interesting ideas are a reward for not being afraid to
    have unconventional beliefs. You can’t grow if you’re
    never willing to turn your back on the status quo. You
    can’t expand if you’re never willing to take an unorthodox
    stand. You can’t have a beautiful mind if you’re never
    willing to leave the crowd behind.\n\n

    It’s easier to fear rejection than it is to open our minds
    to something new, but doing what’s easy doesn’t always
    equal doing what’s authentic, enriching, and meaningful.
`,
  read: 5,
  category: {
    label: 'Enterprise',
    color: 'text-info',
    icon: ''
  },
  author: {
    slug: 'cleber-antonio-fernandes',
    name: 'Cleber Fernandes',
    image: require("../../../assets/img/faces/ayo-ogunseinde-2.jpg"),
    description: `
      Hello guys, nice to have you on the platform! There
      will be a lot of great stuff coming soon. We will
      keep you posted for the latest news.
    `
  },
  kind: '/post',

};

const mockComments = {
  to: '/autor/cleber',
  author: {
    image: require("../../../assets/img/faces/clem-onojeghuo-2.jpg"),
    name: 'Cleber Fernandes'
  },
  crated_at: 'Sep 11, 11:56 AM',
  message: `
    Hello guys, nice to have you on the platform!
    There will be a lot of great stuff coming
    soon. We will keep you posted for the latest
    news. Don't forget, You're Awesome!
  `,
  comments: [
    {
      to: '/autor/cleber',
      author: {
        image: require("../../../assets/img/faces/clem-onojeghuo-2.jpg"),
        name: 'Cleber Fernandes'
      },
      crated_at: 'Sep 11, 11:56 AM',
      message: `
        Hello guys, nice to have you on the platform!
        There will be a lot of great stuff coming
        soon. We will keep you posted for the latest
        news. Don't forget, You're Awesome!
      `,
    },
    {
      to: '/autor/cleber',
      author: {
        image: require("../../../assets/img/faces/clem-onojeghuo-2.jpg"),
        name: 'Cleber Fernandes'
      },
      crated_at: 'Sep 11, 11:56 AM',
      message: `
        Hello guys, nice to have you on the platform!
        There will be a lot of great stuff coming
        soon. We will keep you posted for the latest
        news. Don't forget, You're Awesome!
      `,
      comments: [
        {
          to: '/autor/cleber',
          author: {
            image: require("../../../assets/img/faces/clem-onojeghuo-2.jpg"),
            name: 'Cleber Fernandes'
          },
          crated_at: 'Sep 11, 11:56 AM',
          message: `
            Hello guys, nice to have you on the platform!
            There will be a lot of great stuff coming
            soon. We will keep you posted for the latest
            news. Don't forget, You're Awesome!
          `,
        },
        {
          to: '/autor/cleber',
          author: {
            image: require("../../../assets/img/faces/clem-onojeghuo-2.jpg"),
            name: 'Cleber Fernandes'
          },
          crated_at: 'Sep 11, 11:56 AM',
          message: `
            Hello guys, nice to have you on the platform!
            There will be a lot of great stuff coming
            soon. We will keep you posted for the latest
            news. Don't forget, You're Awesome!
          `,
        }
      ]
    }
  ]
};

function BlogPost({history, match, userAuth}) {

  const [post, setPost] = useState(null);
  const [loading, setLoading] = useState(true);
  const [description, setDescription] = useState(false);
  const [author, setAuthor] = useState(false);
  
  const setTextDescription = (description) => {
    setDescription(StringHelper.handleTextareaSplitPAndTitle(description));
  }  

  const setValues = (postNew) => {
    if (postNew) {
      FirebaseService.getItemFromDatabase('users', postNew.author, setAuthor);
      postNew.prettyDate = DateHelper.dateTime(postNew.updated_at);
      postNew.edited = postNew.updated_at ? 'Editado' : '';
      if (postNew.comments) {
        postNew.comments = GeneralHelpers.objInArray(postNew.comments);
      }
      setPost(postNew);
      setTextDescription(postNew.description);
      setLoading(false);
    } else {
      history.push('/posts');
    }
  }

  const getPost = () => {
    const { id } = match.params;
    if (!post || id !== post.slug) {
      setLoading(true);
      FirebaseService.getItemFromDatabase('posts', id, setValues);
      GeneralHelpers.scrollIntoView(null, 'blog-detail')
    }
  }

  document.documentElement.classList.remove("nav-open");
  useEffect(() => {
    getPost();
    document.body.classList.add("blog-post");
    return function cleanup() {
      document.body.classList.remove("blog-post");
    };
  });

  return (
    <>
      <DangerNavbar navbar={navbar} />
      {/* <BlogPostHeader /> */}
      { !loading ?       
        <div className="wrapper blog-detail">
          <div className="main" >
            <div className="section section-white">

              <GradientHeader>
                <Container>
                  <Row>
                    <Col className="ml-auto mr-auto text-center title" lg="8" md="12" sm="12" xs="12">
                      <h1 >Um lugar para se manter informado</h1>
                      <h3 className="title-uppercase" id="blog-detail">
                        <small>Escrito por corredores para corredores</small>
                      </h3>
                    </Col>
                  </Row>
                </Container>
              </GradientHeader>

              <Container >
                <Row>
                  <Col className="ml-auto mr-auto" lg="10" md="12" sm="12" xs="12">
                    <div className="text-center header-post" >

                      <h3 className="title">
                        {post.title}
                      </h3>

                      <h6 className="title-uppercase">
                        {post.prettyDate} <span className="text-danger">{post.edited}</span>
                      </h6>

                      <Link to={"/posts/" + post.category.label}>
                        <Badge className="main-tag" 
                          color={
                            post.category.background ? 
                              post.category.background :
                              post.category.color.split('-')[1]}>
                          {post.category.icon ? 
                            <i className={"fa mr-1 " + post.category.icon} /> 
                          : null}
                          {post.category.label}
                        </Badge>
                      </Link>

                      { (userAuth && post.author === userAuth.uid) &&
                        <Link className="btn btn-round btn-default btn-edit"
                          to={"/post/form/" + post.slug}>
                          <i className="fa fa-pencil" /> Editar Post
                        </Link>
                      }

                    </div>
                  </Col>
                  <Col className="ml-auto mr-auto comments-read" lg="8" md="12" sm="12" xs="12">

                    { !loading && author &&
                      <InterationAmount
                        commenters={post.commenters}
                        user={userAuth}
                        slug={post.slug}
                        likers={post.likers}
                        savers={post.savers}
                        readers={post.readers}/>
                    }

                    <Card
                      data-radius="none"
                      style={{
                        marginBottom: '10px',
                        backgroundImage:
                          "url(" + post.image + ")"
                      }}/>
                    <p className="image-thumb text-center">
                      Photo by Karina Guedes
                    </p>
                    <div className="article-content">
                      <h4>{post.subtitle}</h4>
                      { description }                     
                    </div>
                    <br />
                    <div className="article-footer">
                      <Container>
                        <Row>
                          <Col md="6">
                            <h5>Tags:</h5>
                            <label className="label label-default mr-1">
                              Motivational
                            </label>
                            <label className="label label-default mr-1">
                              Newsletter
                            </label>
                            <Link to={"/posts/" + post.category.label}>
                              <Badge 
                                className="main-tag" 
                                color={
                                  post.category.background ? 
                                    post.category.background :
                                    post.category.color.split('-')[1]
                                }>
                                {post.category.icon ? 
                                  <i className={"fa mr-1 " + post.category.icon} /> 
                                : null}
                                {post.category.label}
                              </Badge>
                            </Link>
                          </Col>
                          <Col className="ml-auto" md="4">
                            <div className="sharing">
                              <h5>Compartilhe</h5>
                              <Button
                                className="btn-just-icon mr-1"
                                color="twitter">
                                <i className="fa fa-twitter" />
                              </Button>
                              <Button
                                className="btn-just-icon mr-1"
                                color="facebook">
                                <i className="fa fa-facebook" />
                              </Button>
                              <Button className="btn-just-icon" color="google">
                                <i className="fa fa-google" />
                              </Button>
                            </div>
                          </Col>
                        </Row>
                      </Container>
                    </div>
                    <hr />

                    { author && <PostAuthorInfo author={author}/>}

                    <CommentContainer
                      comments={post.comments}
                      slug={post.slug}
                      setValues={setValues}
                      setLoading={setLoading}
                      userAuth={userAuth}/>

                  </Col>
                </Row>

                <RelatedPosts
                  userAuth={userAuth}
                  slug={post.slug}/>

              </Container>
            </div>
          </div>
        </div>
      : null}
      <FooterGray />
    </>
  );
}

export default AuthenticationConsumer(BlogPost);
