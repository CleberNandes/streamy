/** 
 * @author Cleber Antônio Fernandes
 * @email cleber_sim@outlook.com
 * 
 * @props 
 * @bind object useInput
 * @label string 
 * @required boolean
 * @name string
 * @options Array<string> 
 * @value string
*/

import React from 'react'
import { Card, CardBody, CardTitle, CardFooter } from 'reactstrap';
import { Link } from 'react-router-dom';
import StringHelper from '../../helpers/StringHelper';

function CardBackImageDark({title, image, category, slug, kind, description, author}) {

  const _title = StringHelper.capitalize(title);
  
  const cardImage = {
    image: require("../../assets/img/sections/uriel-soberanes.jpg"),
    category: 'Lifestyle',
    title: 'Twenty-Something Travel - Virtual Wayfarer',
    description:`
      As happens in most of these hearings so far, that argument
      led to a back-and-forth between Uber and Waymo’s lawyers
      over the pre-trial discovery...
    `,
    author: {
      slug: 'nome-do-author',
      image: require("../../assets/img/faces/joe-gardner-2.jpg")
    }
  }

  return (
    <Card
      data-background="image"
      style={{ backgroundImage: "url(" + image + ")"
      }}>
      <CardBody>

        <Link to={"/posts/" + category.label}>
          <h6 className={"card-category " + category.color}>
            <i className={"fa " + category.icon} />
            {category.label}
          </h6>
        </Link>

        <Link to={kind + "/" + slug} >
          <CardTitle tag="h3">
            {StringHelper.truncateText(_title, 68)}
          </CardTitle>
        </Link>

        <p className="card-description">
          {StringHelper.truncateText(description, 198)}
        </p>

        <CardFooter>
          <div className="author">
            <Link to={"/autor/" + author.uid} >
              <img
                alt={author.name}
                className="avatar img-raised mr-2"
                src={author.avatar.url}/>
              <span>{author.name}</span>
            </Link>
          </div>
        </CardFooter>
      </CardBody>
    </Card>
  )
}

export default CardBackImageDark;