import React, { useState, useEffect } from 'react'
import { UncontrolledTooltip } from 'reactstrap';
import FirebaseService from '../../app/firebase/Firebase.service';
import GeneralHelpers from '../../helpers/GeneralHelpers';
import { ModalLoginConsumer } from '../../app/pages/sign/ModalLoginContext';

function ReadersCountIcon({readers, user, slug, _setLogin}) {

  let [_readers, setReaders] = useState(readers);
  let [_length, setLength] = useState( GeneralHelpers.arrayLength(_readers) );
  // behavior hide or show icon color
  const [read, setRead] = useState( (_readers) ? !!_readers[user.uid] : false ); 
  
  const addReader = () => {
    setRead(true);
    FirebaseService.getItemFromDatabase('posts', slug + '/readers', setReaders);
    FirebaseService.post(slug).child('readers/' + user.uid)
      .set(user.email).then(() => setLength(GeneralHelpers.arrayLength(_readers) + 1));
  }

  const removeReader = () => {
    FirebaseService.deleteItem('posts', slug + '/readers/' + user.uid);
    FirebaseService.getItemFromDatabase('posts', slug + '/readers', (readers) => {
      setReaders(readers);
      setLength(GeneralHelpers.arrayLength(_readers) - 1);
      setRead(false);
    });
  }

  const readClick = () => {
    if (user) {
      if (!read) {
        addReader()
      } else {
        removeReader();
      }
    } else {
      _setLogin(true)
    }
  }

  const savedScroll = (e) => {
    const currentPosition = window.scrollY || window.pageYOffset;
    const scrollHeight = document.documentElement.scrollHeight;
    const pretendPosition = window.innerHeight;    
    if (currentPosition >= (scrollHeight - pretendPosition)) {
      if (!read && user) {
        addReader();
      }
    }
  }

  useEffect(() => {
    window.addEventListener("scroll", savedScroll);
    return function cleanup() {
      window.removeEventListener('scroll', savedScroll);
    };
  });

  return (
    <div className="amount-item">
      <div id="read"
        className="item-icon-container"
        onClick={readClick}>
        <i className={"fa fa-book "+(read ? 'selected':'')}></i>
      </div>
      <span>{_length ? _length : 0}</span>

      <UncontrolledTooltip placement="top" target="read">
        {read ? 'Marcar como não lido' : 'Marcar como lido'}
      </UncontrolledTooltip>
    </div>
  )
}
export default ModalLoginConsumer(ReadersCountIcon);