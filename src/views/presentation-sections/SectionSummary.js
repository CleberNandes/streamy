import React from "react";

// reactstrap components
import { Container, Row, Col } from "reactstrap";
// core components

function SectionSummary(props) {
  return (
    <>
      <div className="section section-dark section-summary cd-section" id="section-summary">
        <Container>
          <Row>
            {
              props.itens.map((item, i) => {
                return (
                  <Col md="4" key={i}>
                    <div className="info">
                      <div className={item.iconColor}>
                        <i className={item.icon} />
                      </div>
                      <div className="description">
                        <h4 className="info-title">{item.title}</h4>
                        <p>{item.description}</p>
                      </div>
                    </div>
                  </Col>
                )
              })
            }
          </Row>
        </Container>
      </div>
    </>
  );
}

export default SectionSummary;
