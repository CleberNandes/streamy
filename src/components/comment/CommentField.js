import React from 'react'
import InputText from '../form/InputText'
import { Button } from 'reactstrap'
import { AuthenticationConsumer } from '../../app/pages/auth/AuthenticationContext'
import { compose } from 'recompose';
import { ModalLoginConsumer } from '../../app/pages/sign/ModalLoginContext';

function CommentField({comment, save, userAuth, _setLogin}) {

  const checkIfUserAuth = () => {
    
    if (!userAuth) {
      _setLogin(true)
      document.querySelector('.btn-link').focus();
    }
  }

  return (
    <div className="comment-field" onClick={checkIfUserAuth}>

      <InputText
        rows="4"
        placeholder="Digite aqui seu comentário"
        bind={comment.bind}/>
        
      <div className="d-flex flex-row-reverse">

        <Button
          onClick={() => save()}
          className="btn-link btn btn-info mr-1"
          color="danger">
          Salvar
        </Button>

        <Button
          className="btn-link btn btn-info mr-1"
          color="default">
          Cancelar
        </Button>

      </div>
    </div>
  )
}

export default compose (AuthenticationConsumer, ModalLoginConsumer )(CommentField);