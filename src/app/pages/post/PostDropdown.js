import React from 'react'
import {
  DropdownMenu,
  DropdownItem,
  DropdownToggle,
  UncontrolledDropdown
} from "reactstrap";
import { Link } from 'react-router-dom';
import { AuthorModalConsumer } from '../../modal/author-form/AuthorModal';
import { compose } from 'recompose';
import { withRouter } from "react-router-dom";
import { AuthenticationConsumer } from '../auth/AuthenticationContext';
import { checkIfIsAuthor } from '../../services/Author.service';

const PostDropdown = ({userAuth, _setAuthorForm, history}) => {

  return (
    <UncontrolledDropdown className="logged-dropdown" nav inNavbar>

      <DropdownToggle caret nav>
        Artigos
      </DropdownToggle>

      <DropdownMenu className="dropdown-info" right>
        
        <DropdownItem 
          className="d-flex-center" 
          to="/posts" tag={Link}>
          <i className="nc-icon nc-single-copy-04 mr-3" />
          <span>Artigos</span> 
        </DropdownItem>

        { userAuth &&
        <>
          <DropdownItem divider />
          <DropdownItem 
            className="d-flex-center" 
            onClick={() => checkIfIsAuthor(userAuth, _setAuthorForm, history)}>
            <i className="fa fa-camera-retro mr-3" />
            <span>Criar um Artigo</span> 
          </DropdownItem>
        </>
        }

      </DropdownMenu>
    </UncontrolledDropdown>
  )
}
export default compose( AuthenticationConsumer, withRouter, AuthorModalConsumer )(PostDropdown);