import React from "react";
// sections for this page
import SectionSummary from "./presentation-sections/SectionSummary.js";
import SectionComponents from "./presentation-sections/SectionComponents.js";
import SectionCards from "./presentation-sections/SectionCards.js";
import SectionContent from "./presentation-sections/SectionContent.js";
import SectionSections from "./presentation-sections/SectionSections.js";
import SectionExamples from "./presentation-sections/SectionExamples.js";
import SectionIcons from "./presentation-sections/SectionIcons.js";
import SectionFreeDemo from "./presentation-sections/SectionFreeDemo.js";
import SectionResponsive from "./presentation-sections/SectionResponsive.js";
import SectionOverview from "./presentation-sections/SectionOverview.js";
import SectionTestimonials from "./presentation-sections/SectionTestimonials.js";
import SectionSharing from "./presentation-sections/SectionSharing.js";
import VerticalNav from "../components/Navbars/components/VerticalNav";
// data
import navbar from "../data/NavbarData.js"
import sectionSumaryData from "../data/SectionSumaryData";
import ColorNavbar from "../components/Navbars/ColorNavbar.js";
import PresentationHeader from "../components/Headers/PresentationHeader.js";
import FooterBlack from "../components/Footers/FooterBlack.js";
// my comonentes

function Presentation() {
  document.documentElement.classList.remove("nav-open");
  // function that is being called on scroll of the page
  const checkScroll = () => {
    // it takes all the elements that have the .add-animation class on them
    const componentPosition = document.getElementsByClassName("add-animation");
    const scrollPosition = window.pageYOffset;
    for (var i = 0; i < componentPosition.length; i++) {
      var rec =
        componentPosition[i].getBoundingClientRect().top + window.scrollY + 100;
      // when the element with the .add-animation is in the scroll view,
      // the .animated class gets added to it, so it creates a nice fade in animation
      if (scrollPosition + window.innerHeight >= rec) {
        componentPosition[i].classList.add("animated");
        // when the element with the .add-animation is not in the scroll view,
        // the .animated class gets removed from it, so it creates a nice fade out animation
      } else if (scrollPosition + window.innerHeight * 0.8 < rec) {
        componentPosition[i].classList.remove("animated");
      }
    }
  };

  React.useEffect(() => {
    document.body.classList.add("presentation-page");
    window.addEventListener("scroll", checkScroll);
    window.scrollTo(0, 0);
    document.body.scrollTop = 0;
    return function cleanup() {
      document.body.classList.remove("presentation-page");
      window.removeEventListener("scroll", checkScroll);
    };
  });

  const sections = [
    {id: 'presentation-header',  label: 'Presentation Header'},
    {id: 'section-summary',      label: 'Summary'},
    {id: 'section-components',   label: 'Components'},
    {id: 'section-cards',        label: 'Cards'},
    {id: 'section-content',      label: 'Content'},
    {id: 'section-sections',     label: 'Section'},
    {id: 'examples',             label: 'Examples'},
    {id: 'section-icons',        label: 'Icons'},
    {id: 'section-free-demo',    label: 'Free Demo'},
    {id: 'section-responsive',   label: 'Responsive'},
    {id: 'section-overview',     label: 'Overview'},
    {id: 'section-testimonials', label: 'Testimonials'},
    {id: 'demoPay',              label: 'Sharing'}
  ];

  return (
    <>
      <ColorNavbar navbar={navbar}/>
      <PresentationHeader />
      <SectionSummary itens={sectionSumaryData}/>
      <SectionComponents />
      <SectionCards />
      <SectionContent />
      <SectionSections />
      <SectionExamples />
      <SectionIcons />
      <SectionFreeDemo />
      <SectionResponsive />
      <SectionOverview />
      <SectionTestimonials />
      <SectionSharing />
      <VerticalNav sections={sections}/>
      <FooterBlack />
    </>
  );
}

export default Presentation;
