import React, { useEffect } from "react";

// reactstrap components
import {
  Button,
  Container,
  Row,
  Col
} from "reactstrap";

// core components

// data
import navbar from '../../data/NavbarData';
import useInput from "../../components/form/useInput";
import DangerNavbar from "../../components/Navbars/DangerNavbar";
import ImageUpload from "../../components/CustomUpload/ImageUpload";
import TagsInputGroup from "../../components/form/TagsInputGroup";
import InputRadio from "../../components/form/InputRadio";
import InputText from "../../components/form/InputText";
import InputCheckbox from "../../components/form/InputCheckBox";
import FooterBlack from "../../components/Footers/FooterBlack";


function AddProduct() {
  
  const form = {
    name: useInput('', 'text'),
    title: useInput('', 'text'),
    tagline: useInput('', 'text'),
    price: useInput('', 'text'),
    discount: useInput('', 'text'),
    description: useInput('', 'text'),
    spotlight: useInput(false, 'checkbox'),
    agree: useInput(false, 'checkbox'),
    format: useInput('', 'text'),
    tags: useInput(["Minimal","Light","New","Friends"], 'tags'),
    categories: useInput(["Food", "Drink"], 'tags'),
    image: useInput(null),
  };

  const formats = ["Digital", "Print", "Cleber", "Amanda"];
  let fileInput = React.useRef();

  const resetForm = () => {
    for (let item in form) {
      form[item].reset();
    }
  };

  const handleSubmit = () => {
    const data = {};
    for (let item in form) {
      data[item] = form[item].val;
    }
    resetForm();
  };

  const handleSubmitPublish = () => {

  }

  document.documentElement.classList.remove("nav-open");
  useEffect(() => {
    form.image.reset = fileInput.current.handleRemove;
    document.body.classList.add("add-product");
    // window.scrollTo(0, 0);
    // document.body.scrollTop = 0;
    return function cleanup() {
      document.body.classList.remove("add-product");
    };
  });

  return (
    <>
      <DangerNavbar navbar={navbar}/>
      <div className="main">
        <div className="section">
          <Container>
            <h1 className="h3 mb-3">Add Product</h1>
            <div>
              <Row>
                <Col md="5" sm="5">
                  
                  <ImageUpload
                    label="Imagem da Matéria"
                    avatar={false}
                    setImage={form.image.set}
                    image={form.image.val}
                    ref={fileInput}/>

                  <TagsInputGroup
                    label="Tags" 
                    color="badge-success"
                    bind={form.tags.bind}
                    unique={true}/>

                  <TagsInputGroup
                    label="Categories" 
                    color="badge-danger"
                    bind={form.categories.bind}
                    unique={true}/>

                  <InputRadio
                    label="Formato"
                    options={formats}
                    name='formats'
                    value={form.format.val}
                    bind={form.format.bind}
                    required={true}/>

                </Col>
                <Col md="7" sm="7">

                  <InputText
                    placeholder="digite o título do matéria"
                    label="Título"
                    bind={form.title.bind}
                    required={false}
                    showOk={false}/>

                  <InputText 
                    placeholder="digite seu nome aqui"
                    label="Nome"
                    bind={form.name.bind}
                    required={true}
                    showOk={true}/>

                  <InputText
                    placeholder="digite a tag aqui"
                    label="Tagline"
                    bind={form.tagline.bind}
                    required={true}
                    showOk={false}/>

                  <Row>
                    <Col md="6">

                      <InputText 
                        label="Price"
                        placeholder="entre com um preço"
                        iconAddon="fa fa-euro"
                        bind={form.price.bind}
                        required={true}
                        showOk={true}/>

                    </Col>
                    <Col md="6">

                      <InputText
                        label="Discount"
                        placeholder="enter discount"
                        iconAddon="fa fa-percent"
                        bind={form.discount.bind}
                        required={true}
                        showOk={true}/>

                    </Col>
                  </Row>

                  <InputText
                    label="Description"
                    type="textarea"
                    rows="6"
                    maxLength="150"
                    placeholder="This is a textarea limited to 150 characters."
                    required={true}
                    bind={form.description.bind}/>

                  <InputCheckbox
                    label="Display on landing page"
                    required={true}
                    bind={form.spotlight.bind}/>
                  
                  <InputCheckbox 
                    label="Agree with the terms"
                    required={true}
                    bind={form.agree.bind}/>
                  
                </Col>
              </Row>
              <Row>
                <Col md="4" sm="4">
                  <Button
                    block
                    className="btn-round"
                    color="danger"
                    outline
                    onClick={() => resetForm()}
                    type="reset">
                    Cancel
                  </Button>
                </Col>
                <Col md="4" sm="4">
                  <Button
                    block
                    className="btn-round"
                    color="primary"
                    outline
                    onClick={() => handleSubmit()}
                    type="submit">
                    Save
                  </Button>
                </Col>
                <Col md="4" sm="4">
                  <Button
                    block
                    className="btn-round"
                    color="primary"
                    onClick={() => handleSubmitPublish()}
                    type="submit">
                    Save &amp; {'&'} Publish
                  </Button>
                </Col>
              </Row>
            </div>
          </Container>
        </div>
      </div>
      <FooterBlack />
    </>
  );
}

export default AddProduct;
