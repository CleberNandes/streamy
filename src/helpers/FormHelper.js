

const resetForm = (form) => {
  for (let item in form) {
    form[item].reset();
  }
};

const getDataFromForm = (form) => {
  const data = [];
  for (let item in form) {
    data[item] = form[item].val;
  }
  return data;
}

const hasError = form => {
  let error = false;
  markAsTouched(form);
  for(let item in form) {
    if (!form[item].valid) {
      error = true;
    }
  }
  return error;
}

const markAsTouched = form => {
  for(let item in form) {
    form[item].bind.onBlur();
  }
}

export default {
  resetForm,
  getDataFromForm,
  hasError,
  markAsTouched
};