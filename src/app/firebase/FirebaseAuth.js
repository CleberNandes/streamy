import Firebase from "./FirebaseConfig";

export const createUser = (email, password) => {
  return Firebase.auth.createUserWithEmailAndPassword(email, password)
}

export const signIn = (email, password) => {
  return Firebase.auth.signInWithEmailAndPassword(email, password);
}

export const signOut = () => Firebase.auth.signOut();

export const passwordReset = email => Firebase.auth.sendPasswordResetEmail(email);

export const passwordUpdate = password =>
    Firebase.auth.currentUser.updatePassword(password);

export const updateEmail = email =>
    Firebase.auth.currentUser.updateEmail(email);

export const sendEmailVerification = () =>
    Firebase.auth.currentUser.sendEmailVerification();

export const deleteUser = () =>
    Firebase.auth.currentUser.delete();

export const updateProfile = (displayName, photoURL) => 
  Firebase.auth.currentUser.updateProfile({displayName, photoURL});

export const currentUser = () => { return Firebase.auth.currentUser() };

export default {
  createUser,
  signIn,
  signOut,
  passwordReset,
  passwordUpdate,
  currentUser
}

