const navbar = {
  color: {
    change: false,
    maxheight: 500,
    // can be, danger, info, white is default don't need to pass anything
    // color: 'danger'
  },
  brand: {
    to: '/index',
    name: 'Paper Kit PRO React',
    logo: '',
    tooltip: true
  },
  dropdowns: [
    {
      section: 'Multilevel Dropdown',
      color: 'default',
      colorDropdown: 'dropdown-danger',
      itens: [
        { label: 'Action', to: '/index' },
        { label: 'Another action'  , to: '/presentation' },
        {
          sublevel: true, 
          itens: [{
            sublevel: true, 
            section: 'Submenu',
            color: 'default',
            colorDropdown: 'dropdown-danger',
            itens: [
              { label: 'Submenu action', to: '/index' },
              { label: 'Submenu action', to: '/index' },
              {
                sublevel: true,
                itens: [{
                  sublevel: true,
                  section: 'Subsubmenu',
                  color: 'default',
                  colorDropdown: 'dropdown-danger',
                  itens: [
                    { label: 'Submenu action 1', to: '/index' },
                    { label: 'Submenu action 1', to: '/index' },
                  ]
                }]
              },
              {
                sublevel: true, 
                itens: [{
                  sublevel: true, 
                  section: 'Submenu',
                  color: 'default',
                  colorDropdown: 'dropdown-danger',
                  itens: [
                    { label: 'Submenu action', to: '/index' },
                    { label: 'Submenu action', to: '/index' },
                    {
                      sublevel: true, 
                      itens: [{
                        sublevel: true, 
                        section: 'Subsubmenu',
                        color: 'default',
                        colorDropdown: 'dropdown-danger',
                        itens: [
                          { label: 'Submenu action 1', to: '/index' },
                          { label: 'Submenu action 1', to: '/index' },
                        ]
                      }]
                    }
                  ]
                }]
              }
            ]
          }]
        }
      ]
    },
    {
      section: 'Components',
      color: 'default',
      colorDropdown: 'dropdown-danger',
      itens: [
        { label: 'All Components', to: '/index' },
        { label: 'Presentation'  , to: '/presentation' },
        {
          label: 'Documentation',
          href: 'https://demos.creative-tim.com/paper-kit-pro-react/#/documentation/introduction?ref=pkpr-color-navbar',
          target: '_blank'
        }
      ]
    },
    {
      section: 'Sections',
      color: 'default',
      colorDropdown: 'dropdown-danger',
      itens: [
        { label: 'Headers'     , to: '/sections#headers'     , icon: 'nc-icon nc-tile-56' },
        { label: 'Features'    , to: '/sections#features'    , icon: 'nc-icon nc-settings' },
        { label: 'Blogs'       , to: '/sections#blogs'       , icon: 'nc-icon nc-bullet-list-67' },
        { label: 'Teams'       , to: '/sections#teams'       , icon: 'nc-icon nc-single-02' },
        { label: 'Projects'    , to: '/sections#projects'    , icon: 'nc-icon nc-calendar-60' },
        { label: 'Pricing'     , to: '/sections#pricing'     , icon: 'nc-icon nc-money-coins' },
        { label: 'Testimonials', to: '/sections#testimonials', icon: 'nc-icon nc-badge' },
        { label: 'Contact Us'  , to: '/sections#contact-us'  , icon: 'nc-icon nc-mobile' },
      ]
    },
    {
      section: 'Examples',
      color: 'default',
      colorDropdown: 'dropdown-danger',
      itens: [
        { label: 'About-us'     , to: '/about-us'           , icon: 'nc-icon nc-bank' },
        { label: 'Add Product'  , to: '/add-product'        , icon: 'nc-icon nc-basket' },
        { label: 'Blog Post'    , to: '/blog-post'          , icon: 'nc-icon nc-badge' },
        { label: 'Blog Posts'   , to: '/blog-posts'         , icon: 'nc-icon bullet-list-67' },
        { label: 'Contact Us'   , to: '/contact-us'         , icon: 'nc-icon nc-mobile' },
        { label: 'Discover'     , to: '/discover'           , icon: 'nc-icon nc-world-2' },
        { label: 'Ecommerce'    , to: '/e-commerce'         , icon: 'nc-icon nc-send' },
        { label: 'Landing Page' , to: '/landing-page'       , icon: 'nc-icon nc-spaceship' },
        { label: 'Login Page'   , to: '/login'              , icon: 'nc-icon nc-lock-circle-open' },
        { label: 'Product Page' , to: '/product-page'       , icon: 'nc-icon nc-album-2' },
        { label: 'Profile Page' , to: '/profile-page'       , icon: 'nc-icon nc-single-02' },
        { label: 'Register Page', to: '/cadastro'           , icon: 'nc-icon nc-bookmark-2' },
        { label: 'Search Page'  , to: '/search-with-sidebar', icon: 'nc-icon nc-zoom-split' },
        { label: 'Settings Page', to: '/settings'           , icon: 'nc-icon nc-settings-gear-65' },
        { label: 'Twitter'      , to: '/twitter-redesign'   , icon: 'nc-icon nc-tie-bow' },
      ]
    }
  ],
  navItens: [
    {
      class: 'btn-round',
      color: 'danger',
      href: 'https://www.creative-tim.com/product/paper-kit-pro-react?ref=pkpr-white-navbar',
      label: 'Buy now',
      icon: {
        class: 'nc-icon nc-cart-simple',
        label: 'Buy now'
      }
    }
  ]
}
  

  export default navbar;