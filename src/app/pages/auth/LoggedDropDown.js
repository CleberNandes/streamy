import React from 'react';
import {
  DropdownMenu,
  DropdownItem,
  DropdownToggle,
  UncontrolledDropdown
} from "reactstrap";
import { Link } from 'react-router-dom';
import { AuthenticationConsumer } from './AuthenticationContext';
import LogoutButton from '../sign/LogoutButton';
import { AvatarProfileConsumer } from '../../modal/image-profile/AvatarProfileModal';
import { compose } from 'recompose';
import defaultAvatar from "../../../assets/img/placeholder.jpg";

const LoggedDropDown = ({userAuth, _setOpen}) => {

  return (
    <UncontrolledDropdown className="logged-dropdown" nav inNavbar>

      <DropdownToggle className="avatar" caret nav>
        <img
          alt={userAuth.name}
          className="avatar img-raised"
          src={userAuth.avatar ? userAuth.avatar.url : defaultAvatar}/>
      </DropdownToggle>

      <DropdownMenu className="dropdown-info" right>

        <DropdownItem className="text-center" header>
          {userAuth.name}
        </DropdownItem>

        <DropdownItem divider />
        
        <DropdownItem 
          className="d-flex-center" 
          to={"/autor/" + userAuth.uid} tag={Link}>
          <i className="nc-icon nc-single-copy-04 mr-3" />
          <span className="">
            Perfil
          </span> 
        </DropdownItem>

        <DropdownItem divider />

        <DropdownItem 
          className="d-flex-center" 
          onClick={() => _setOpen(true)}>
          <i className="fa fa-camera-retro mr-3" />
          <span className="">
            Mudar avatar
          </span> 
        </DropdownItem>

        <DropdownItem divider />

        <LogoutButton />

      </DropdownMenu>
    </UncontrolledDropdown>
  )
}
export default compose( AuthenticationConsumer, AvatarProfileConsumer )(LoggedDropDown);