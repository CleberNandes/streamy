import Firebase from "./FirebaseConfig";
import GeneralHelpers from "../../helpers/GeneralHelpers";

export const saveDatabase = (table, name, data) => {
  return Firebase.database
    .ref(table)
    .child(name)
    .set(data);
}

export const saveImageStorage = (path, image, createFunction) => {
  Firebase
    .storage
    .ref(path)
    .child(image.lastModified + image.name)
    .put(image)
    .on('state_changed', 
      (snapShot) =>  console.log(snapShot), 
      (error) => console.error(error), 
      () => {
        Firebase
          .storage
          .ref(path)
          .child(image.lastModified + image.name)
          .getDownloadURL()
          .then(url => {
            createFunction(url);
          });
    });
}

export const deleteImageStorage = (path, image) => {
  
  return Firebase
    .storage
    .ref(path)
    .child(image)
    .delete();
}

export const getListFromDatabase = (list, field, callback) => {
  Firebase
    .database
    .ref(list)
    .orderByChild(field)
    .once("value")
    .then(snapshot => {
      let itens = GeneralHelpers.objInArray(snapshot.val());
      callback(itens)
    })
}

export const getListCategory = (list, field, value, callback) => {
  Firebase
    .database
    .ref(list)
    .orderByChild(field)
    .equalTo(value)
    .once("value")
    .then(snapshot => {
      let itens = GeneralHelpers.objInArray(snapshot.val());
      callback(itens)
    })
}

export const getItemFromDatabase = (list, slug, callback) => {
  Firebase
    .database
    .ref(list)
    .child(slug)
    .once("value")
    .then(snapshot => callback(snapshot.val()))
}

export const relatedsLimit = (list, field, not, limit, callback, user) => {
  Firebase
    .database
    .ref(list)
    .orderByChild(field)
    .once("value")
    .then(snapshot => {
      let itens = GeneralHelpers.objInArray(snapshot.val());      
      itens = itens.filter(item => item.slug !== not)
      let newestFirst = [];
      if (user) {
        newestFirst = itens.filter( item => item.readers ? !item.readers[user.uid] : true )
      }
      newestFirst = itens.slice(0, limit);
      if (newestFirst.length < 1) {
        newestFirst = itens.slice(0, limit);
      }
      callback(newestFirst)
    })
}

export const deleteItem = (list, slug) => {
  Firebase
    .database
    .ref(list)
    .child(slug)
    .set(null);
}

// *** User API ***
export const user = uid => Firebase.database.ref(`users/${uid}`);
export const users = () => Firebase.database.ref('users');
// *** Post API
export const post = uid => Firebase.database.ref(`posts/${uid}`);
export const posts = () => Firebase.database.ref('posts');

export default {
  saveDatabase, 
  saveImageStorage,
  deleteImageStorage,
  getListFromDatabase,
  getListCategory,
  getItemFromDatabase,
  relatedsLimit,
  deleteItem,
  user, users,
  post, posts
}