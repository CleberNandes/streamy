import React, { useEffect, useState } from 'react'
import { Button, Row, Col } from "reactstrap";
import InputText from '../../../components/form/InputText';
import useInput from "../../../components/form/useInput";
import InputCheckbox from "../../../components/form/InputCheckBox";
import CategoryService from "../../services/Category.service";
import InputRadio from "../../../components/form/InputRadio";
import { AuthenticationConsumer } from '../../pages/auth/AuthenticationContext';
import AvatarProfileForm from '../image-profile/AvatarProfileForm';
import FirebaseService from '../../firebase/Firebase.service';
import FormHelper from '../../../helpers/FormHelper';

function AuthorForm({userAuth, modal}) {

  const categories = CategoryService.all();
  const [goToForm, setGoToForm] = useState(false);
  const form = {
    firstName: useInput(userAuth.firstName || '', 'text', {required: '', minLength: 4}),
    lastName: useInput(userAuth.lastName || '', 'text', {required: '', minLength: 4}),
    facebook: useInput(userAuth.facebook || '', 'text', {required: '', startWith: 'http', minLength: 20}),
    instagram: useInput(userAuth.instagram || '', 'text', {required: '', startWith: 'http', minLength: 20}),
    description: useInput(userAuth.description || '', 'textarea', {required: '', minLength: 100}),
    agreePotitics: useInput(userAuth.agreePotitics || '', 'checkbox', {required: ''}),
    category: useInput(userAuth.category && userAuth.category.label, 'text', {required: ''}),
    image: useInput(userAuth.image)
  }

  const resetForm = () => {
    for (let item in form) {
      form[item].reset();
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (FormHelper.hasError(form)) return;
    const data = prepareData();
    FirebaseService.user(userAuth.uid).update(data).then(() => modal(false))
  };

  const prepareData = () => {
    const data = {};
    for (let item in form) {
      if (form[item].val) {
        data[item] = form[item].val;      
      }
    }
    data.category = CategoryService.getCategory(data.category);
    data.author = true;
    return data;
  }

  let fileInput = React.useRef();
  useEffect(() => {
    if (fileInput.current) {
      form.image.reset = fileInput.current.handleRemove;
    }    
  });

  if (!goToForm) {
    return (
      <div className="text-center fileinput">
        <h2 className="mb-2">Junte-se a nós e seja um colunista</h2>
        <h5>Somos um lugar para compartilhar conhecimento e se manter atualizado.</h5>
        <div>
          <img 
            className="thumbnail img-circle mt-4 mb-4"
            alt="artigo de assessoria esportiva" 
            src={require("../../../assets/img/oficial/posts/write-an-article.png")}/>
        </div>
        <h5>Para ser um colunista você tem que prencher mais algumas informações.</h5>
        <Button onClick={() => setGoToForm(true)}
          className="btn-round mt-4"
          color="primary"
          outline>
          Prencher informações para ser colunista
        </Button>
      </div>
    )
  }

  return (
    <form onSubmit={handleSubmit}>
      <div className="d-flex align-items-center">
        <h3 className="mb-3 flex-fill">Formulário de Colunista</h3>
        <Button
          onClick={resetForm}
          className="btn-round mr-3"
          color="default"
          outline
          type="button">
          cancelar
        </Button>
        <Button
          className="btn-round"
          color="primary"
          outline
          type="submit">
          Salvar
        </Button>
      </div>

      <p>Preencher todas as informações.</p>

      <Row>
        <InputText
          className="col-sm-6 col-xs-12"
          placeholder="digite apenas o primeiro nome"
          label="Primeiro nome"
          field={form.firstName}/>

        <InputText
          className="col-sm-6 col-xs-12"
          placeholder="digite seu sobrenome preferido"
          label="Melhor sobrenome"
          field={form.lastName}/>
      </Row>

      <Row>
        <InputText
          className="col-sm-6 col-xs-12"
          placeholder="digite uma breve descrição a seu respeito"
          label="Sua descrição"
          rows="5"
          maxLength="400"
          field={form.description}/>

        <Col sm="6" xs="12">

          <InputText
            placeholder="endereço do seu facebook"
            label="Facebook"
            field={form.facebook}/>

          <InputText
            placeholder="endereço do seu instagram"
            label="Instagram"
            field={form.instagram}/>
        </Col>
      </Row>


      <Row>
        <Col sm="7" xs="12">

          <InputRadio
            label="Tema preferido"
            options={categories}
            name='categories'
            field={form.category}/>

        </Col>
        <Col sm="5" xs="12">

          <AvatarProfileForm size="size100px"/>
            
        </Col>
      </Row>
      
      <InputCheckbox
        label="Li e concordo com a política de Las Corredoras"
        field={form.agreePotitics}/>

    </form>
  );
}

export default AuthenticationConsumer(AuthorForm);