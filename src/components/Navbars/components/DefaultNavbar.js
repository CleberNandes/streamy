import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
// nodejs library that concatenates strings
import classnames from "classnames";
// JavaScript plugin that hides or shows a component based on your scroll
import Headroom from "headroom.js";
// reactstrap components
import {
  Button,
  Collapse,
  NavbarBrand,
  Navbar,
  NavLink,
  NavItem,
  Nav,
  Container,
  UncontrolledTooltip
} from "reactstrap";
import NavDropdownList from "./NavDropdownList";
import { compose } from 'recompose';
import { AuthenticationConsumer } from "../../../app/pages/auth/AuthenticationContext";
import { ModalLoginConsumer } from "../../../app/pages/sign/ModalLoginContext";
import LoggedDropDown from "../../../app/pages/auth/LoggedDropDown";
import PostDropdown from '../../../app/pages/post/PostDropdown';

function DefaultNavbar({ navbar, userAuth, _setLogin }) {  
  
  const [navbarColor, setNavbarColor] = useState("navbar-transparent"); 
  const [bodyClick, setBodyClick] = useState(false);
  const [collapseOpen, setCollapseOpen] = useState(false);

  useEffect(() => {
    let headroom = new Headroom(document.getElementById("navbar-main"));
    headroom.init();

    if(navbar.color.change) {
      const updateNavbarColor = () => {
        if (
          document.documentElement.scrollTop > navbar.color.maxheight ||
          document.body.scrollTop > navbar.color.maxheight
        ) {
          setNavbarColor("");
        } else if (
          document.documentElement.scrollTop <= navbar.color.maxheight ||
          document.body.scrollTop <= navbar.color.maxheight
        ) {
          setNavbarColor("navbar-transparent");
        }
      };
      window.addEventListener("scroll", updateNavbarColor);
      return function cleanup() {
        window.removeEventListener("scroll", updateNavbarColor);
      };
    }

  });

  return (

    <>
      {bodyClick ? (
        <div
          id="bodyClick"
          onClick={() => {
            document.documentElement.classList.toggle("nav-open");
            setBodyClick(false);
            setCollapseOpen(false);
          }}
        />
      ) : null}
      <Navbar 
        {...(navbar.color.color ? {color: navbar.color.color} : {})}
        className={classnames("fixed-top", navbar.color.change ? navbarColor : null)} 
        expand="lg" 
        id="navbar-main">

        <Container>
          <div className="navbar-translate">

            <NavbarBrand id="navbar-brand" to={navbar.brand.to} tag={Link}>
              {navbar.brand.logo ? 
                <img src={navbar.brand.logo} alt={navbar.brand.name} /> 
                : navbar.brand.name
              }
            </NavbarBrand>

            { navbar.brand.tooltip ? 
              <UncontrolledTooltip placement="bottom" target="navbar-brand">
                {navbar.brand.name}
              </UncontrolledTooltip> : null
            }

            <button
              className="navbar-toggler"
              id="navigation"
              type="button"
              onClick={() => {
                document.documentElement.classList.toggle("nav-open");
                setBodyClick(true);
                setCollapseOpen(true);
              }}>
              <span className="navbar-toggler-bar bar1" />
              <span className="navbar-toggler-bar bar2" />
              <span className="navbar-toggler-bar bar3" />
            </button>
          </div>
          <Collapse navbar isOpen={collapseOpen}>
            <Nav className="ml-auto" navbar>

              <NavDropdownList navItens={navbar.dropdowns} />

              <PostDropdown />

              { !userAuth && 
                <NavItem
                  color="info"
                  onClick={() => _setLogin(true)}>
                  <NavLink>
                    Login
                  </NavLink>
                </NavItem>
              }
              
              { userAuth && <LoggedDropDown /> }

              {navbar.navitens ? navbar.navItens.map(item => (
                <NavItem>
                  <Button
                    className={item.class}
                    color={item.color}
                    {...(item.to ? {to: item.to, tag: Link} : {})}
                    {...(item.href ? {href: item.href, target: '_blank'} : {})}>
                    <i className={item.icon.class} /> {item.icon.label}
                  </Button>
                </NavItem>
              )) : null}
              
            </Nav>
          </Collapse>
        </Container>
      </Navbar>
    </>

  );
}

export default compose(
  AuthenticationConsumer,
  ModalLoginConsumer
)(DefaultNavbar);