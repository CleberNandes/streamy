import React from 'react';
import { Route, Redirect } from "react-router-dom";

function NotAuth({ Component, authUser, ...rest }) {
  // const counter = useSelector(state => state);
  console.log(authUser);
  
  return (
    
    <Route
      {...rest}
      render={props =>
        authUser ? 
          <Redirect to="/presentation" />
          :
          <Component {...{...props, ...rest}} /> 
      }
    />
  );
}
export default NotAuth;