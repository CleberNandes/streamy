import React from 'react'

const GradientHeader = (props) => {

  const color1 = props.color1 ? props.color1 : '#7935A6';
  const color2 = props.color2 ? props.color2 : '#D4489E';
  // const heigth = props.height ? props.height : '300px';

  return (
    <div className="gradient-section"
      style={{background: `linear-gradient(to right bottom, ${color1}, ${color2})`}}>
      {props.children}
    </div>
  )
}

export default GradientHeader;