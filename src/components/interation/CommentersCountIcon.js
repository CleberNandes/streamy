import React from 'react'
import { UncontrolledTooltip } from 'reactstrap';
import GeneralHelpers from '../../helpers/GeneralHelpers';

export default function SaversCountIcon({commenters, user}) {


  
  const _commenters = GeneralHelpers.objInArray(commenters);
  console.log(commenters);
  const comment = _commenters ? !!_commenters.find(item => item === user.uid) : false;
  const _length = GeneralHelpers.arrayLength(_commenters);

  return (
    <div className="amount-item">
      <div id="commenters"
        className="item-icon-container">
        <i className={"fa fa-commenting "+(comment ? 'selected':'')}></i>
      </div>
      <span>{_length ? _length : 0}</span>

      <UncontrolledTooltip placement="top" target="commenters">
        {comment ? 'você comentou' : 'Você não comentou'}
      </UncontrolledTooltip>
    </div>
  )
}