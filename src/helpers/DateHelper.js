const options = { month: "long", day: "numeric", year: "numeric" };

const generateDate = () => {
  // retorna 2020-02-29T05:37:48.224Z
  return new Date().toISOString();
};

// formas de usar o getPretty
// getPretty(now.toISOString());
// getPretty(now.toUTCString());
// retorna 29 de fevereiro de 2020
const getPretty = (date) => {
  return new Date(date).toLocaleDateString("pt-BR", options);
};

// return 29/02/2020 02:46
const dateTime = (date) => {
  return new Date(date).toLocaleDateString("pt-BR", 
  { day: "numeric", month: "numeric", year: "2-digit", hour: 'numeric', minute: 'numeric' });
}

// return 29/02/2020
const dateFormatted = (date) => {
  return new Date(date).toLocaleDateString();
}

// return 842398724398
export const dateId = () => {
  return new Date().getTime();
}

export default { generateDate, getPretty, dateTime, dateFormatted, dateId };