import React from 'react';
import TagsInput from 'react-tagsinput';
import classNames from 'classnames';
import { FormGroup } from 'reactstrap';

/**
* Returns a stateful value, and a function to update it.
* props:
* label="Categories"
* value={categories}
* color="badge-danger
* set={setCategories}
* unique={true}/
* ploaceholder="string"
*/
function TagsInputGroup(props) {
 
  const placeholder = props.placeholder ? props.placeholder : 'Adicionar uma tag';
  
  return (
    <FormGroup>
      <label>{props.label}</label>
      <div id={props.label}>
        <TagsInput
          tagProps={{
            className: classNames("react-tagsinput-tag", props.color)
          }}
          inputProps={{
            placeholder
          }}
          {...(props.unique ? {onlyUnique: true} : {})}
          onChange={props.bind.onChange}
          value={props.bind.value ? props.bind.value : []}
        />
      </div>
    </FormGroup>
  );
}

export default TagsInputGroup;