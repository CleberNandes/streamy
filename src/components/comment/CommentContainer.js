import React, { createRef, useState, useEffect } from 'react'
import CommentField from './CommentField';
import useInput from '../form/useInput';
import Comment from './Comment';
import DateHelper from '../../helpers/DateHelper';
import FirebaseService from '../../app/firebase/Firebase.service';

function CommentContainer({userAuth, setValues, setLoading, slug, comments}) {

  let commentsRef = createRef();
  const comment = useInput("", 'textarea');
  let [users, setUsers] = useState([]);

  const getUsers = () => {
    if (!users.length) {
      FirebaseService.getListFromDatabase('users', 'created_at', setUsers);
    }
  }

  const chargeComment = () => {
    return (
      <CommentField 
        comment={comment}
        save={saveComment}/>
    )
  }

  const saveComment = () => {
    if (comment.val) {
      const data = {};
      data.message = comment.val;      
      data.author = userAuth.uid;
      data.created_at = DateHelper.generateDate();
      data.id = new Date().getTime().toString();
      saveFirebase('comments/' + data.id, data, data.id)
    }
  }

  const saveCommentUser = (id) => {
    FirebaseService.saveDatabase(
      'posts/' + slug, 'commenters/' + id, userAuth.uid)
  }

  const saveFirebase = (path, data, id) => {
    setLoading(false);
    FirebaseService.saveDatabase('posts/' + slug, path, data)
      .then(() => { 
        comment.reset(); 
        saveCommentUser(id);
        FirebaseService.getItemFromDatabase('posts', slug, setValues);
      });
  }

  useEffect(() => {
    getUsers();
  })

  return (
    <div className="comments media-area"
      ref={commentsRef}>
      <h3 className="text-center">Comentários</h3>

      { chargeComment() }

      {comments && users.length ?
        comments.map((item, i) => {
          return <Comment key={i}
            author={users.find(user => user.uid === item.author)}
            userAuth={userAuth}
            users={users}
            to={item.to}
            message={item.message}
            created_at={item.created_at}
            comments={item.comments}
            add={chargeComment}
            saveFirebase={saveFirebase}
            path={'comments/' + item.id}/>
          })
      : null}

    </div>
  )
}
export default CommentContainer;