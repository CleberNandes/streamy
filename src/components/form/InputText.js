/** 
 * @author Cleber Antônio Fernandes
 * @email cleber_sim@outlook.com
 * @howToUse useInput(false, () => fieldName.set(!fieldName.val))
 * @label string 
 * @placeholder string 
 * @field object useInput
 * @error string
 * @type string | textarea transform
 * @row string
 * @maxLength string
 * @iconAddon string | transform inputGroup
 * 
*/

import React from 'react'
import { Input, InputGroupAddon, InputGroupText, InputGroup, Label } from 'reactstrap';

function InputText({field, type, iconAddon, className, label, rows, maxLength, placeholder}) {

  const typeInput = field.bind.type ? field.bind.type : type ? type : 'text';
  const HasIcon = iconAddon ? InputGroup : 'div';  

  return (
    <div className={
      "form-group " + 
      (className ? className : '') +
      (field.error ? ' is-invalid' : '')}>
      
      <Label
        htmlFor={label}>
        {label}
      </Label>

      <HasIcon className="border-input">

        <Input 
          style={{ resize: "none" }}
          type={typeInput}
          id={label}
          {...(rows ? {rows: rows} : {})}
          {...(maxLength ? {maxLength: maxLength} : {})}
          placeholder={placeholder}
          value={field.bind.value ? field.bind.value : ''}
          onChange={field.bind.onChange}
          onBlur={field.bind.onBlur}
          onKeyUp={field.bind.onKeyUp}
          />

          { maxLength && !field.error ? 
            <small
              id="textarea-limited-message">
              {(field.bind.value ? field.bind.value.length : 0) + ' / ' + maxLength}
            </small> 
          : null }

        { iconAddon ? 
            <InputGroupAddon addonType="append">
              <InputGroupText>
                <i className={iconAddon} />
              </InputGroupText>
            </InputGroupAddon>
          : null }
      </HasIcon>

      { field.error ? 
        <small className="invalid-feedback">
          {field.error}
        </small> 
      : null }

    </div>
  );
}

export default InputText;