import React, { useState } from 'react';
import { Media, Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import useInput from '../form/useInput';
import CommentField from './CommentField';
import DateHelper from '../../helpers/DateHelper';
import GeneralHelpers from '../../helpers/GeneralHelpers';


function Comment({saveFirebase, path, to, author, created_at, message, comments, add, userAuth, users}) {
  
  const comment = useInput("", 'textarea');
  const [showComment, setShowComment] = useState(false);

  const getCommentField = (e) => {
    setShowComment(!showComment);
  }

  const commentField = () => {
    return <CommentField comment={comment} save={saveComment}/>
  }

  const saveComment = () => {
    if (comment.val) {
      const data = {};
      data.message = comment.val;
      data.author = userAuth.uid
      data.created_at = DateHelper.generateDate();
      data.id = new Date().getTime().toString();
      saveFirebase(path + '/comments/' + data.id, data, data.id)
      setShowComment(false);
    }
  }

  return (
    <Media /* onClick={() => showComment && hideCommentField()} */>
      <Link
        className="pull-left"
        to={to + "/autor/" + author.uid}>
        <div className="avatar">
          <Media object
            alt={author.name}
            src={author.avatar.url}/>
        </div>
      </Link>
      <Media body>

        <Media heading tag="h5">
          {author.name}
        </Media>

        <div className="pull-right">
          <h6 className="text-muted media-date">
            {DateHelper.dateTime(created_at)}
          </h6>
          
          <Button
            className="btn-link"
            color="info"
            onClick={() => getCommentField()}>
            <i className="fa fa-reply mr-1" />
            Responder
          </Button>
        </div>
        <p>{message}</p>

        {/* aqui devo colocar respostas desse comentário */}
        <div className="comment-container">
          {showComment ? commentField() : null}
        </div>

        { comments ? 
          GeneralHelpers.objInArray(comments).map((item, i) => 
            <Comment key={i}
              author={users.find(user => user.uid === item.author)}
              userAuth={userAuth}
              users={users}
              to={item.to}
              message={item.message}
              created_at={item.created_at}
              comments={GeneralHelpers.objInArray(item.comments)}
              add={add}
              saveFirebase={saveFirebase}
              path={path + '/comments/' + item.id}/>
          )
        : null}
      </Media>
    </Media>
  )
}

export default Comment;