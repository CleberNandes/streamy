import React, { useState, useEffect, createContext } from 'react';
import FirebaseConfig from '../../firebase/FirebaseConfig';
import FirebaseService from '../../firebase/Firebase.service';
export let UserAuthContext = createContext(null);

export const AuthenticationProvider = Component => {
  
  const Provider = (props) => {

    const [authUser, setAuthUser] = useState(null);

    const prepareLoggedUser = (currentUser) => {
      if ( currentUser ) {
        FirebaseService.user(currentUser.uid)
        .on('value', snapshot => {
          const val = snapshot.val();
          
          for(let prop in val) {
            if (!currentUser[prop]) {
              currentUser[prop] = val[prop];
            }
          }
          currentUser.avatar = val.avatar;          
          setAuthUser(currentUser);
        }, error => console.error(error))
      } else setAuthUser(null);
    }

    useEffect(() => {
      const subscribeAuth = FirebaseConfig.auth.onAuthStateChanged(currentUser => {
        prepareLoggedUser(currentUser);
      });
      return () => {
        subscribeAuth();
        authUser && FirebaseService.user(authUser.uid).off();
      }
    }, [authUser]);

    return (
      <UserAuthContext.Provider value={authUser}>
        <Component {...{...props, authUser}} />
      </UserAuthContext.Provider>
    );
  }
  return Provider;
};

export const AuthenticationConsumer = Component => {
  const Consumer = (props) => 
    <UserAuthContext.Consumer>
    {
      userAuth => 
      <Component {...{...props, userAuth}} />
    }
    </UserAuthContext.Consumer>
  return Consumer;
};