import React from "react";
import { Link } from "react-router-dom";
// reactstrap components
import {
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown
} from "reactstrap";

function NavDropdownList(props) {

  return (
    <>
      {props.navItens ? props.navItens.map((item) => {
        return (
          <UncontrolledDropdown
            {...(!item.sublevel ? {nav: true, inNavbar: true} : {})}
            key={props.navItens.indexOf(item).toString()}>
              
            <DropdownToggle 
              color={item.color} caret 
              {...(item.sublevel ? {className: 'dropdown-item', tag: 'a'} : {nav: true})}>
              {item.section}
            </DropdownToggle>

            <DropdownMenu 
              className={item.colorDropdown} 
              {...(!item.sublevel ? {right: true} : {})}>
              {item.itens ? item.itens.map((dropItem) =>
                dropItem.sublevel ? 
                  <NavDropdownList navItens={dropItem.itens} 
                    key={item.itens.indexOf(dropItem).toString()}/>
                  : (
                  <DropdownItem 
                    key={item.itens.indexOf(dropItem).toString()}
                    {...(dropItem.to ? {to: dropItem.to, tag: Link} : {})}
                    {...(dropItem.href ? {href: dropItem.href} : {})}
                    {...(dropItem.target ? {target: dropItem.target} : {})}>
                    {dropItem.icon ? <i className={dropItem.icon} /> : ''}
                    {dropItem.label}
                  </DropdownItem>
                )
              ) : null}
              
            </DropdownMenu>
          </UncontrolledDropdown>
        )
      }) : null}
    </>
  );
}

export default NavDropdownList;
