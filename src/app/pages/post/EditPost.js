
import React, { useState, useRef } from "react";
import { Container } from "reactstrap";
import FormPost from "./FormPost";
import FirebaseService from "../../firebase/Firebase.service";
import DateHelper from "../../../helpers/DateHelper";
import navbar from "../../../data/NavbarData";
import DangerNavbar from "../../../components/Navbars/DangerNavbar";
import { AuthenticationConsumer } from "../auth/AuthenticationContext";


function Edit({ match, history, userAuth }){
  
  const { id } = match.params;
  const formRef = useRef();
  
  const [post, setPost] = useState(null);

  // só será usado no edit post
  if (id && (!post || !post.title)) {
    FirebaseService.getItemFromDatabase('posts', id, setPost);
  }

  const editPost = (imgUrl) => {
    const data = formRef.current.prepareData(imgUrl);
    data.updated_at = DateHelper.generateDate();
    FirebaseService.post(data.slug).update(data)
      .then(() => {
        formRef.current.resetForm();
        history.push(`/post/` + id)
      });
  };
  
  if (!post) {
    return <h1>loading ...</h1>
  }
  const clone = {...post};
  clone.category = clone.category.label;

  return (
    <>
      <DangerNavbar {...{navbar}}/>
      <div className="main">
        <div className="section">
          <Container>
            <h1 className="h2 mb-3">Editar um novo post</h1>
            <FormPost
              ref={formRef}
              save={editPost}
              post={clone}/>
          </Container>
        </div>
      </div>
    </>
  );
}

export default AuthenticationConsumer(Edit);