import React, { useEffect, useState } from "react";
import { Button } from 'reactstrap';

const id = '1061826626040-3egvausvi598bll7kqpoaqtlvbphasnu.apps.googleusercontent.com';
const key = 'q101GLNCEKVQgNqIg34SFaXo';

function GoogleAuthBtn({round}) {

  const [auth, setAuth] = useState(null);
  const [isSignedIn , setIsSignedIn] = useState(null);

  const handleClick = () => {
    if (auth.isSignedIn.get()) {
      auth.signOut();
    } else {
      auth.signIn();
    }
  }

  const initGoogleAuth = () => {
    window.gapi.load('client:auth2', () => {
      window.gapi.client.init({ clientId: id, scope: 'email' })
        .then(() => {
          setAuth(window.gapi.auth2.getAuthInstance());
          setIsSignedIn(window.gapi.auth2.getAuthInstance().isSignedIn.get());
          window.gapi.auth2.getAuthInstance().isSignedIn.listen(onAuthChange);
          
        })
    });
  }

  const onAuthChange = call => {
    setIsSignedIn(call);
  }

  useEffect(() => {
    if (!auth) {
      initGoogleAuth();
    }
  })

  return (
    <>
      { auth ? 
        !round ?
          <Button className="btn-just-icon mr-1" 
            color="google"
            onClick={handleClick}>
            <i className="fa fa-google" />
          </Button>
          :
          <Button className="btn-round mr-1" 
            color="google"
            onClick={handleClick}>
            <i className="fa fa-google mr-2" />
            { !isSignedIn ? 'login com google' : 'logado'}
          </Button>
      : null
      }
    </>
  );
  
}

export default GoogleAuthBtn;