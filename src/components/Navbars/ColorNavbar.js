import React from "react";
import DefaultNavbar from './components/DefaultNavbar';

const ColorNavbar = ({navbar, authUser}) => {  
  navbar.color = {
    change: true,
    color: ''
  }

  return <DefaultNavbar {...{authUser, navbar}}/>;
}

export default ColorNavbar;
