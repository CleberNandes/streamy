import React from 'react'
import { Button, Card, CardTitle, Form} from "reactstrap";
import useInput from '../../../components/form/useInput';
import FormHelper from '../../../helpers/FormHelper';
import FirebaseAuth from '../../firebase/FirebaseAuth';
import InputText from '../../../components/form/InputText';

function PasswordChangeForm() {

  const form = {
    password: useInput('', 'text'),
    confirm: useInput('', 'text')
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    const data = FormHelper.getDataFromForm(form);
    FirebaseAuth.passwordUpdate(data.password)
      .then(authUser => {
        console.log(authUser);
        FormHelper.resetForm(form);
        // history.push('/autor/jorge-antonio-fernandes');
      }).catch(error => {
        console.log(error);
      });    
  }

  return (
    <Card className="card-register card-login">
      <CardTitle tag="h3">Welcome</CardTitle>

      <Form className="register-form" onSubmit={handleSubmit}>
        
        <InputText
          className="no-border"
          placeholder="digite sua senha..."
          label="Senha"
          bind={form.password.bind}
          required={true}
          showOk={false}/>
        
        <InputText
          className="no-border"
          placeholder="confirme sua senha..."
          label="Confirmação de Senha"
          bind={form.confirm.bind}
          required={true}
          showOk={false}/>

        <Button block 
          className="btn-round"
          type="submit"
          color="danger">
          Trocar senha
        </Button>
      </Form>

    </Card>
  )
}
export default PasswordChangeForm;