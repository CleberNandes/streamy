import React, { useState } from 'react'
import { Button, CardTitle, Form} from "reactstrap";
import useInput from '../../../components/form/useInput';
import FormHelper from '../../../helpers/FormHelper';
import DateHelper from '../../../helpers/DateHelper';
import FirebaseAuth from '../../firebase/FirebaseAuth';
import InputText from '../../../components/form/InputText';
import Loader from '../../../components/Loader';

function LoginForm({history, setLoginFormActive, setLogin}) {
  
  const [_loading, _setLoading] = useState(false);

  const form = {
    email: useInput('', 'email', {required: '', email: ''}),
    password: useInput('', 'password', {required: '', password: ''})
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    if (FormHelper.hasError(form)) return;
    _setLoading(true);
    const data = FormHelper.getDataFromForm(form);
    data.logged_at = DateHelper.generateDate();
    console.log(data);
    FirebaseAuth.signIn(data.email, data.password)
      .then(authUser => {
        console.log(authUser);
        FormHelper.resetForm(form);
        history.push('/autor/jorge-antonio-fernandes');
      }).catch(error => {
        console.log(error);
        if (error.code === 'auth/user-not-found') {
          form.email.setError('Nenhum usuário encontrado com esse email!')
        }
        else if (error.code === 'auth/too-many-requests') {
          form.email.setError('Muitas tentativas, tente mais tarde!')
        }
        else if (error.code === 'auth/wrong-password') {
          form.password.setError('Senha inválida!')
        }
      })
      .finally(() => {
        _setLoading(false);
        setLogin && setLogin(false)
      });
  }

  return (
    <>
      <Loader loading={_loading} />
      
      <CardTitle tag="h3">Welcome</CardTitle>

      <Form className="register-form" onSubmit={handleSubmit}>

        <InputText
          className="no-border"
          placeholder="digite seu email..."
          label="Email"
          field={form.email}/>
        
        <InputText
          className="no-border"
          placeholder="digite sua senha..."
          label="Senha"
          field={form.password}/>

        <Button block 
          className="btn-round"
          type="submit"
          color="danger">
          Register
        </Button>
      </Form>

      <div className="forgot">

        <Button
          className="btn-link"
          color="danger"
          onClick={() => setLoginFormActive && setLoginFormActive(false)}>
            Esqueceu a senha?
        </Button>

        <Button
          className="btn-link"
          color="danger"
          onClick={() => true}>
            Criar uma conta?
        </Button>

      </div>

    </>
  )
}
export default LoginForm;