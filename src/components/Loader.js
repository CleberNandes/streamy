import React from "react";
import { css } from "@emotion/core";
import ClipLoader from "react-spinners/ClipLoader";
 
// Can be a string as well. Need to ensure each key-value pair ends with ;
 
const Loader = ({loading}) => {

  const override = css`
    // display: block;
    // margin: 0 auto;
    // border-color: red;
  `;

  const color = "#ffffff";
  const size = 50;

  if (!loading) {
    return null;
  }

  return (
    <div className="loader-component">
      <ClipLoader
        size={size}
        // css={objeto}
        color={color}
        loading={loading}/>
    </div>
  );
}

export default Loader;