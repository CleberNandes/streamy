import React from "react";
import { BrowserRouter, Route, Redirect, Switch } from "react-router-dom";

// styles
import "../assets/css/bootstrap.min.css";
import "../assets/scss/paper-kit.scss";
import "../assets/demo/demo.css";
import "../assets/demo/react-demo.css";
import 'fontawesome';
// pages
import Index from "../views/Index";
import NucleoIcons from "../views/NucleoIcons";
import Sections from "../views/Sections";
import Presentation from "../views/Presentation";
import AboutUs from "../views/examples/AboutUs";
import AddProduct from "../views/examples/AddProduct";

import BlogPosts from "../views/examples/BlogPosts";
import ContactUs from "../views/examples/ContactUs";
import Discover from "../views/examples/Discover";
import Ecommerce from "../views/examples/Ecommerce";
import Error404 from "../views/examples/Error404";
import Error422 from "../views/examples/Error422";
import Error500 from "../views/examples/Error500";
import LandingPage from "../views/examples/LandingPage";
import ProductPage from "../views/examples/ProductPage";
import ProfilePage from "../views/examples/ProfilePage";

import SearchWithSidebar from "../views/examples/SearchWithSidebar";
import Settings from "../views/examples/Settings";
import TwitterRedesign from "../views/examples/TwitterRedesign";
// my app
import LoginPage from "./pages/sign/LoginPage";
import Home from "./pages/home/Home";
import CreatePost from "./pages/post/CreatePost";
import EditPost from "./pages/post/EditPost";
import PostsDeCorrida from "./pages/post/PostsDeCorrida";
import BlogPost from "./pages/post/BlogPost";
import RegisterPage from "./pages/register/RegisterPage";
import Auth from "./pages/auth/Auth";
import NotAuth from "./pages/auth/NotAuth";
import AdminPage from "./admin/AdminPage";
import { compose } from 'recompose';
import { AvatarProfileProvider } from "./modal/image-profile/AvatarProfileModal";
import { AuthenticationProvider } from "./pages/auth/AuthenticationContext";
import { ModalLoginProvider } from "./pages/sign/ModalLoginContext";
import { AuthorModalProvider } from "./modal/author-form/AuthorModal";

const App = ({authUser}) => {

  return (
  
    <BrowserRouter>
      <Switch>
        {/* admin */}
        <Route path="/admin"     render={props => <AdminPage {...({authUser, ...props})} />} />
        {/* minha aplicação */}
        <Route path="/home"             render={props => <Home {...({authUser, ...props})} />} />
        <Route path="/presentation"     render={props => <Presentation {...({authUser, ...props})} />} />
        <Route path="/sections"         render={props => <Sections {...({authUser, ...props})} />} />
        <Route path="/landing-page"     render={props => <LandingPage {...({authUser, ...props})} />} />
        <Route path="/about-us"         render={props => <AboutUs {...({authUser, ...props})} />} />
        <Route path="/contact-us"       render={props => <ContactUs {...({authUser, ...props})} />} />
        {/* logado */}
        <Auth path="/post/form/:id" Component={EditPost}      {...{authUser}} />
        <Auth path="/post/form"     Component={CreatePost}    {...{authUser}} />
        {/* deslogado */}
        <NotAuth path="/login"      Component={LoginPage}     {...{authUser}} />
        <NotAuth path="/cadastro"   Component={RegisterPage}  {...{authUser}} />
        {/* Posts */}
        <Route path="/post/:id"         render={props => <BlogPost {...({authUser, ...props})} />}/>
        <Route path="/posts/:category"  render={props => <PostsDeCorrida {...({authUser, ...props})} />} />
        <Route path="/posts"            render={props => <PostsDeCorrida {...({authUser, ...props})} />} />
        {/* profile */}
        <Route path="/autor/:id"        render={props => <ProfilePage {...({authUser, ...props})} />} />
        <Route path="/profile-page"     render={props => <ProfilePage {...({authUser, ...props})} />} />
        <Route path="/settings"            render={props => <Settings {...({authUser, ...props})} />} />
        {/* error */}
        <Route path="/error-404"           render={props => <Error404 {...({authUser, ...props})} />} />
        <Route path="/error-422"           render={props => <Error422 {...({authUser, ...props})} />} />
        <Route path="/error-500"           render={props => <Error500 {...({authUser, ...props})} />} />
        {/* framework */}
        <Route path="/index"               render={props => <Index {...({authUser, ...props})} />} />
        <Route path="/nucleo-icons"        render={props => <NucleoIcons {...({authUser, ...props})} />} />
        <Route path="/add-product"         render={props => <AddProduct {...({authUser, ...props})} />} />
        <Route path="/blog-post"           render={props => <BlogPost {...({authUser, ...props})} />} />
        <Route path="/blog-posts"          render={props => <BlogPosts {...({authUser, ...props})} />} />
        <Route path="/discover"            render={props => <Discover {...({authUser, ...props})} />} />
        <Route path="/e-commerce"          render={props => <Ecommerce {...({authUser, ...props})} />} />
        <Route path="/product-page"        render={props => <ProductPage {...({authUser, ...props})} />} />
        <Route path="/search-with-sidebar" render={props => <SearchWithSidebar {...({authUser, ...props})} />} />
        <admin path="/twitter-redesign"    render={props => <TwitterRedesign {...({authUser, ...props})} />} />
        <Redirect to="/presentation" />
      </Switch>
    </BrowserRouter>
  )
}

export default compose(
  AuthenticationProvider,
  ModalLoginProvider,
  AvatarProfileProvider,
  AuthorModalProvider
)(App);