import React, { useState } from 'react'
import { UncontrolledTooltip } from 'reactstrap';
import FirebaseService from '../../app/firebase/Firebase.service';
import GeneralHelpers from '../../helpers/GeneralHelpers';
import { ModalLoginConsumer } from '../../app/pages/sign/ModalLoginContext';

function SaversCountIcon({savers, user, slug, _setLogin}) {

  const [_savers, _setSavers] = useState(savers);
  let [_length, setLength] = useState( GeneralHelpers.arrayLength(_savers) );
  const [_save, _setSave] = useState(_savers ? !!_savers[user.uid] : false);

  const addSaver = () => {
    _setSave(true);
    FirebaseService.getItemFromDatabase('posts', slug + '/savers', _setSavers);
    FirebaseService.post(slug).child('savers/' + user.uid)
      .set(user.email).then(() => setLength(GeneralHelpers.arrayLength(_savers) + 1));
  }

  const removeSaver = () => {
    FirebaseService.deleteItem('posts', slug + '/savers/' + user.uid);
    FirebaseService.getItemFromDatabase('posts', slug + '/savers', (savers) => {
      _setSavers(savers);
      setLength(GeneralHelpers.arrayLength(_savers) - 1);
      _setSave(false);
    });

  }

  const saveClick = () => {
    if (user) {
      if (!_save) {
        addSaver()
      } else {
        removeSaver();
      }
    } else {
      _setLogin(true)
    }
  }

  return (
    <div className="amount-item">
      <div id="savers"
        className="item-icon-container"
        onClick={saveClick}>
        <i className={"fa fa-thumb-tack "+(_save ? 'selected':'')}></i>
      </div>
      <span>{_length ? _length : 0}</span>

      <UncontrolledTooltip placement="top" target="savers">
        {_save ? 'Tirar da sua lista' : 'Salvar na sua lista'}
      </UncontrolledTooltip>
    </div>
  )
}
export default ModalLoginConsumer(SaversCountIcon);