import React from 'react';
import GeneralHelpers from '../../../helpers/GeneralHelpers';


function VerticalNav(props) {

  // para usar essa classe é preciso colocar a classe cd-section nas seções para serem mapeadas

  React.useEffect(() => {
    
    window.addEventListener("scroll", updateView);
    return () => {
      window.removeEventListener("scroll", updateView);
    } 
  });

  const updateView = () => {    
    var contentSections = document.getElementsByClassName("cd-section");
    var navigationItems = document
      .getElementById("cd-vertical-nav")
      .getElementsByTagName("a");

    for (let i = 0; i < contentSections.length; i++) {
      var activeSection =
        parseInt(navigationItems[i].getAttribute("data-number"), 10) - 1;
      if (
        contentSections[i].offsetTop - window.innerHeight / 2 <
          window.pageYOffset &&
        contentSections[i].offsetTop +
          contentSections[i].scrollHeight -
          window.innerHeight / 2 >
          window.pageYOffset
      ) {
        navigationItems[activeSection].classList.add("is-selected");
      } else {
        navigationItems[activeSection].classList.remove("is-selected");
      }
    }
  };

  return (
    <nav id="cd-vertical-nav">
      <ul>
        {props.sections.map((item, i) => {
          return (
            <li key={i}>
              <a
                data-number={i + 1}
                href={'#' + item.id}
                onClick={(e) => GeneralHelpers.scrollIntoView(e, item.id)}>
                <span className="cd-dot" />
                <span className="cd-label">{item.label}</span>
              </a>
            </li>
          )
        })}
      </ul>
    </nav>
  );
  
}

export default VerticalNav;