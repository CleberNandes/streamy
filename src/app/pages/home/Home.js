
import React, { useState } from "react";



// This is new.
import Firebase from "../../firebase/FirebaseConfig";
import ColorNavbar from "../../../components/Navbars/ColorNavbar";
import navbar from "../../../data/NavbarData";

function Home() {
  // blog read
  const [loading, setLoading] = useState(true);
  const [blogPosts, setBlogPosts] = useState([]);
  if (loading && !blogPosts.length) {
    Firebase.get()
      .database()
      .ref("/posts")
      .orderByChild("date")
      .once("value")
      .then(snapshot => {
        let posts = [];
        const snapshotVal = snapshot.val();
        for (let slug in snapshotVal) {
          posts.push(snapshotVal[slug]);
        }

        const newestFirst = posts.reverse();
        setBlogPosts(newestFirst);
        setLoading(false);
      });
  }

  if (loading) {
    return <h1>Loading...</h1>;
  }
  // blog read till here

  return (
    <>
      <ColorNavbar navbar={navbar}/>
    </>
  );
}

export default Home;