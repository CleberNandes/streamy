import React from "react";
import DefaultNavbar from './components/DefaultNavbar';

function WhiteNavbar(props) {

  props.navbar.color = {
    change: false,
    color: ''
  }

  return <DefaultNavbar navbar={props.navbar}/>;
}

export default WhiteNavbar;
