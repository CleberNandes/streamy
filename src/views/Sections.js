import React from "react";
// reactstrap components
// sections for page
import SectionHeader from "./sections-sections/SectionHeader.js";
import SectionFeature from "./sections-sections/SectionFeature.js";
import SectionBlog from "./sections-sections/SectionBlog.js";
import SectionTeam from "./sections-sections/SectionTeam.js";
import SectionProject from "./sections-sections/SectionProject.js";
import SectionPricing from "./sections-sections/SectionPricing.js";
import SectionTestimonials from "./sections-sections/SectionTestimonials.js";
import SectionContactUs from "./sections-sections/SectionContactUs.js";
// data
import navbar from "../data/NavbarData";
// my componentes
import VerticalNav from '../components/Navbars/components/VerticalNav'
import WhiteNavbar from "../components/Navbars/WhiteNavbar.js";
import FooterBlack from "../components/Footers/FooterBlack.js";

function Sections() {

  let verticalNav = React.useRef();
  const sections = [
    {id: 'headers',      label: 'Headers'},
    {id: 'features',     label: 'Features'},
    {id: 'blogs',        label: 'Blogs'},
    {id: 'teams',        label: 'Teams'},
    {id: 'projects',     label: 'Projects'},
    {id: 'pricing',      label: 'Pricing'},
    {id: 'testimonials', label: 'Testimonials'},
    {id: 'contact-us',   label: 'Contact Us'},
  ];

  React.useEffect(() => {
    document.documentElement.classList.remove("nav-open");
    document.body.classList.add("section-page");

    var href = window.location.href.substring(
      window.location.href.lastIndexOf("#/") + 2
    );
    if (href.lastIndexOf("#") > 0) {      
      // pega o id depois da hash
      var hrefId = href.substring(href.lastIndexOf("#") + 1);
      document.getElementById(hrefId).scrollIntoView({
        behavior: "smooth",
        block: "start",
        inline: "nearest"
      });
    }
    
    return () => {
      document.body.classList.remove("section-page");
    }

  });

  return (
    <>
      <WhiteNavbar navbar={navbar}/>
      <div className="section-space" />
      <SectionHeader />
      <SectionFeature />
      <SectionBlog />
      <SectionTeam />
      <SectionProject />
      <SectionPricing />
      <SectionTestimonials />
      <SectionContactUs />
      <VerticalNav sections={sections} />
      <FooterBlack />
    </>
  );
  
}

export default Sections;

