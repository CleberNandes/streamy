import React, { useState } from 'react'
import ImageUpload from '../../../components/CustomUpload/ImageUpload';
import { AuthenticationConsumer } from '../../pages/auth/AuthenticationContext';
import ImageService from '../../services/Image.service';
import FirebaseService from '../../firebase/Firebase.service';
import { dateId } from '../../../helpers/DateHelper';

const AvatarProfileForm = ({userAuth, size}) => {
  
  const [avatar, setAvatar] = useState(userAuth ? userAuth.avatar : null);  

  const handleChange = (img) => {
    setAvatar(img);
    checkIfExistImage(img);
  };

  const saveImage = (imgUrl, img) => {
    const date = dateId().toString();    
    FirebaseService.user(userAuth.uid)
      .update({avatar: {
        url: imgUrl, 
        name: date + img.name
      }})
      .then((item) => console.log(item) );
  }

  const removeImage = (img) => {    
    ImageService.remove('users', img)
      .then((item) => console.log('image deleted', item) )
      .catch(error => console.log(error) )
  }

  const checkIfExistImage = (img) => {    
    FirebaseService.user(userAuth.uid)
      .once('value').then((snapshot) => {
        const user = snapshot.val();
        if (user && user.avatar) {
          removeImage(user.avatar.name)
        }        
        ImageService.save('users', img, (url) => saveImage(url, img));
      });
  }

  return (
    <div className="avatar-form">            
      <ImageUpload
        label="Imagem da Capa"
        avatar={true}
        setImage={handleChange}
        image={avatar && avatar.url}
        showButtons={false}
        size={size}
        // ref={fileInput}
      />
    </div>
  )
}

export default AuthenticationConsumer(AvatarProfileForm);