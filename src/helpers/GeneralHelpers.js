import NumberHelper from "./NumberHelper";

export const readTime = (text) => {    
  const words = text.split(' ').length;    
  return NumberHelper.roundUp(words / 180);
}

export const scrollIntoView = (e, id) => {
  e && e.preventDefault();
  const el = document.getElementById(id);
  if (el) {
    el.scrollIntoView({
      behavior: "smooth",
      block: "start",
      inline: "nearest"
    });
  }
};

export const objInArray = (snapshotVal) => {
  let itens = [];

  for (let slug in snapshotVal) {
    itens.push(snapshotVal[slug]);
  }
  return itens.reverse();
}

export const arrayLength = (list) => {
  let itens = 0;
  for (let slug in list) {
    slug && itens ++;
  }
  return itens;
}

export default {
  readTime,
  scrollIntoView,
  objInArray,
  arrayLength
}