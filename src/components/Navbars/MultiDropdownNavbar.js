import React from "react";
import DefaultNavbar from './components/DefaultNavbar';

// core components

function MultiDropdownNavbar(props) {
  
  return <DefaultNavbar navbar={props.navbar}/>;
}

export default MultiDropdownNavbar;

