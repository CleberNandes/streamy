/** 
 * @author Cleber Antônio Fernandes
 * @email cleber_sim@outlook.com
 * @howToUse useInput('', ({target: {value}}) => field.set(value)),
 * @props 
 * @label string 
 * @avatar boolean
 * @setImage function
 * @alt string
 * @image file
 * @changeLabel string button 
 * @removeLabel string button
*/

import React, { forwardRef, useImperativeHandle } from "react";
// used for making the prop types of this component
import PropTypes from "prop-types";

import { Button } from "reactstrap";

import defaultImage from "../../assets/img/image_placeholder.jpg";
import defaultAvatar from "../../assets/img/placeholder.jpg";

const ImageUpload = forwardRef((props, ref) => {
  // tem que passar image value e setImage set
  const [imagePreviewUrl, setImagePreviewUrl] = React.useState(
    props.image ? props.image :
    props.avatar ? defaultAvatar : defaultImage
  );
  const showButtons = (props.showButtons === false) ? props.showButtons : true;
  const showLabel = (props.showLabel === false) ? props.showLabel : true;
  const fileInput = React.createRef();

  const handleImageChange = e => {

    e && e.preventDefault();
    const reader = new FileReader();
    const file = e.target.files[0];
    if (file) {
      reader.onloadend = () => {
        // verificar se passo o file ou reader.result(que é em base 64)
        props.setImage(file);
        setImagePreviewUrl(reader.result);
      };
      reader.readAsDataURL(file);
    }
  };

  const handleClick = () => {
    fileInput.current.click();
  };

  const handleRemove = () => {
    props.setImage(null);
    setImagePreviewUrl(props.avatar ? defaultAvatar : defaultImage);
    if (fileInput.current) {
      fileInput.current.value = null;
    }
  };

  useImperativeHandle(ref, () => ({
    handleRemove: () => handleRemove()
  }));
  
  return (
    <>

      { showLabel && 
        <label className="d-block">
          {props.label ? props.label : 'Imagem'}
        </label>
      }

      <div className="fileinput text-center">
        <input 
          type="file" 
          onChange={handleImageChange} 
          ref={fileInput} />

        <div 
          className={"thumbnail " + (props.avatar ? " img-circle " : "") + (props.size ? props.size : '')}
          onClick={handleClick}>
          <img 
            src={imagePreviewUrl} 
            alt={props.alt ? props.alt : 'imagem'} />
          
          { props.avatar && 
            <div className="hover-img">
              <i className="fa fa-camera-retro" />
            </div>
          }
        </div>

        { showButtons && 
          <div>
            {props.image === null ?
              <Button
                className="btn-round"
                color="default"
                outline
                onClick={handleClick}>
                { props.avatar ? "Adicionar Foto" : "Selecione uma Imagem" }
              </Button>
            :
              <span>
                <Button
                  className="btn-round"
                  outline
                  color="default"
                  onClick={handleClick}>
                  { props.changeLabel ? props.changeLabel : 'mudar' }
                </Button>

                {props.avatar ? <br /> : null}

                <Button
                  color="danger"
                  className="btn-round btn-link"
                  onClick={handleRemove}>
                  <i className="fa fa-times" />
                  { props.removeLabel ? props.removeLabel : 'remover' }
                </Button>
              </span>
            }
          </div>
        }
      </div>
    </>
  );
});

ImageUpload.propTypes = {
  avatar: PropTypes.bool,
  showButtons: PropTypes.bool
};

export default ImageUpload;
