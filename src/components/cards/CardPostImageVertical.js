/** 
 * @author Cleber Antônio Fernandes
 * @email cleber_sim@outlook.com
 * 
 * @props 
 * @bind object useInput
 * @label string 
 * @required boolean
 * @name string
 * @options Array<string> 
 * @value string
*/

import React from 'react';
import { Card, CardBody, CardTitle, CardFooter, UncontrolledTooltip } from 'reactstrap';
import { Link } from 'react-router-dom';
import classnames from "classnames";
import StringHelper from '../../helpers/StringHelper';
import defaultAvatar from "../../assets/img/placeholder.jpg";

const CardPostImageVertical = ({
    className, slug, kind, imageAlt, image, canEdit, readPost, 
    category, title, subtitle, author, readTime, userAuth
  }) => {
  
  return (
    <Card className={"card-vertical " + className}>

      <div className="card-image">
        <Link to={kind + "/" + slug}>
          <img
            alt={imageAlt}
            className="img"
            src={image}
          />
        </Link>
        
      </div>

      <CardBody>

        { 
        (canEdit && userAuth) && 
          <Link className="btn btn-round btn-default btn-edit"
            id={"a" + slug}
            to={kind + "/form/" + slug}>
            <i className="fa fa-pencil" /> 
            <UncontrolledTooltip placement="bottom" target={"a" + slug}>
              Edite seu Artigo
            </UncontrolledTooltip>
          </Link>
        }

        {(readPost && userAuth) && 
          <div id={"s" + slug}
            className="icon-read item-icon-container">
            <i className={"fa fa-book selected"}></i>
            <UncontrolledTooltip placement="top" target={"s" + slug}>
              Artigo lido
            </UncontrolledTooltip>
          </div>
        }

        <Link to={kind + "s/" + category.label}>
          <h6  className={classnames('card-category', category.color)}>
            { category.icon ? 
              <i className={classnames('fa', category.icon)} /> 
            : null}
            {category.label}
          </h6>
        </Link>

        <Link to={kind + "/" + slug}>
          <CardTitle tag="h5">
            {StringHelper.truncateText(title, 47)}
          </CardTitle>
        </Link>

        <p className="card-description">
          {StringHelper.truncateText(subtitle, 124)}
        </p>

        <hr />

        <CardFooter>
          <div className="author">
            <Link to={"/autor/" + author.uid}>
              <img
                alt={author.name}
                className="avatar img-raised mr-2"
                src={author.avatar ? author.avatar.url : defaultAvatar}/>
              <span>{author.firstName + ' ' + author.lastName}</span>
            </Link>
          </div>

          <div className="stats">
            <i className="fa fa-clock-o" /> {readTime} min <i className="fas fa-book-open" />
          </div>

        </CardFooter>
      </CardBody>
    </Card>
  )
}
export default CardPostImageVertical;