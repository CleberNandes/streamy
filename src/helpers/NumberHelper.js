

export const round = (number) => {
  return Math.round(number);
}

export const roundDown = (number) => {
  return Math.trunc(number)
}

export const roundUp = (number) => {
  return Math.round(number);
}

export default {
  round,
  roundDown,
  roundUp
}