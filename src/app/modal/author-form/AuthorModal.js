import React, { useState, createContext } from 'react';
import { Modal } from 'reactstrap';
import AuthorForm from './AuthorForm';
export let AuthorContext = createContext(null);

export const AuthorModalProvider = Component => {
  const Provider = (props) => {
    const [_author, _setAuthorForm] = useState(false);
    return (
      <AuthorContext.Provider value={_setAuthorForm}>
        <Component {...{...props}} />
        <Modal
          size="lg"
          isOpen={_author}
          toggle={() => _setAuthorForm(false)}
          className="">

          <div className="modal-body">
            <button
              className="close"
              type="button"
              onClick={() => _setAuthorForm(false)}>
              <span>×</span>
            </button>
            <AuthorForm modal={_setAuthorForm}/>
          </div>
        </Modal>
      </AuthorContext.Provider>
    );
    
  }
  return Provider;
};

export const AuthorModalConsumer = Component => {
  const Consumer = (props) => {
    return (
      <AuthorContext.Consumer >
        { _setAuthorForm => 
          <Component {...{...props, _setAuthorForm}} />
        }
      </AuthorContext.Consumer>
    );
  }
  return Consumer;
};