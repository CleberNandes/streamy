import React, { useState, createContext } from 'react';
import { Modal } from 'reactstrap';
import AvatarProfileForm from './AvatarProfileForm';

export let AvatarProfileContext = createContext(null);

export const AvatarProfileProvider = Component => {
  const Provider = (props) => {    
    const [_open, _setOpen] = useState(false);    
    

    return (
      <AvatarProfileContext.Provider value={_setOpen}>
        <Component {...{...props}} />
        <Modal
          isOpen={_open}
          toggle={() => _setOpen(false)}
          className="modal-register text-center">

          <div className="modal-body">
            <button
              className="close"
              type="button"
              onClick={() => _setOpen(false)}>
              <span>×</span>
            </button>

            <h3>Imagem do perfil</h3>
            
            <hr/>

            <AvatarProfileForm />
            
          </div>
        </Modal>
      </AvatarProfileContext.Provider>
    );
  }
  return Provider;
};

export const AvatarProfileConsumer = Component => {
  const Consumer = (props) => {
    return (
      <AvatarProfileContext.Consumer>{
        _setOpen => <Component {...{...props, _setOpen}} />
      }</AvatarProfileContext.Consumer>
    );
  }
  return Consumer;
};