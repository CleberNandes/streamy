/**
 * Returns a stateful value, a function to update it,
 * a reset method to update and a bind object with
 * value, checked and onChange function whose to use have
 * to pass a callback
 * 
 * @author Cleber Fernandes 
 */

import { useState } from 'react';

const useInput = (initialValue, type, validation = {}, compare) => {
  const [val, set] = useState(initialValue);
  const [error, setError] = useState(null);
  const [touched, setTouched] = useState(null);
  const [valid, setValid] = useState(null);

  const getOnChangeFuncByType = (type, set, val) => {
    if (type === 'text' || type === 'textarea' || type === 'password' || type === 'email') {
      return ({target: {value}}) => set(value);
    } else if (type === 'checkbox') {
      return () => set(!val)
    } else if (type === 'tags') {
      return (value) => set(value)
    }
  }

  const validateField = (val, validation, setError, compare) => {
    
    let confirm = null;
    for( let item in validation) {      
      switch(item) {
        case 'required':
          confirm = !!val.length;
          confirm || setError('Campo obrigatório.');
          break;
        case 'minLength':
          confirm = val.length >= validation[item];
          confirm || setError(`O campo deve ter no mínimo ${validation[item]} digitos.`);
          break;
        case 'maxLength':
          confirm = val.length <= validation[item];
          confirm || setError(`O campo deve ter no máximo ${validation[item]} digitos.`);
          break;
        case 'email':
          confirm = !!val.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
          confirm || setError('Email inválido.');
          break;
        case 'password':
          confirm = val.length >= 6;
          confirm || setError('Senha deve ter 6 digitos.');
          break;
        case 'confirm':
          confirm = val === compare.val;
          confirm || setError('A confirmação está diferente da senha.');
          break;
        case 'startWith':
          confirm = val.slice(0, validation[item].length) === validation[item];
          confirm || setError(`O endereço deve começar com ${validation[item]}`);
          break;
        default: break;
      }
      if (!confirm) {
        setValid(false)
        break;
      };
    }
    if (confirm) {
      setError('');
      setValid(true);
    }
    setTouched(true);
  }

  return {
    val, 
    set, 
    error,
    setError,
    valid,
    reset: () => set(''),
    bind: {
      type,
      value: val,
      checked: val,
      onChange: getOnChangeFuncByType(type, set),
      onBlur: () => validateField(val, validation, setError, compare),
      onKeyUp: () => {
        touched && validateField(val, validation, setError, compare);
        setError('');
      }
    }
  }
}

export default useInput;