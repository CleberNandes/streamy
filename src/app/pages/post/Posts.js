import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import { compose } from 'recompose';
import { withRouter } from "react-router-dom";

import FirebaseService from '../../firebase/Firebase.service';
import CardPostImageVertical from '../../../components/cards/CardPostImageVertical';
import { AuthorModalConsumer } from '../../modal/author-form/AuthorModal';
import { checkIfIsAuthor } from '../../services/Author.service';

const Posts = ({category, userAuth, history, _setAuthorForm}) => {  

  const [loading, setLoading] = useState(true);
  const [posts, setPosts] = useState([]);
  const [users, setUsers] = useState([]);
  const [_category, setCategory] = useState(category);  

  const setInitial = () => {
    getUsers();
    getPosts();

    if (_category !== category) {
      FirebaseService.getListCategory('posts', 'category/label', category, setPosts);
    }
    setLoading(false);
  }

  const getPosts = () => {
    if (loading && !posts.length) {
      if (category) {
        FirebaseService.getListCategory('posts', 'category/label', category, setPosts);
      } else {
        FirebaseService.getListFromDatabase('posts', 'created_at', setPosts);
      }
    }
  }

  const getUsers = () => {
    if (loading && !users.length) {
      FirebaseService.getListFromDatabase('users', 'created_at', setUsers);
    }
  }

  useEffect(() => {
    setInitial();
  });

  if (loading) {
    return <h1>Loading...</h1>;
  }

  return (
    <div className="blog-2 section section-gray">
      <Container>
        <div className="d-flex align-items-center">
          <h2 className="title flex-fill">Artigos sobre Corrida</h2>

          { userAuth &&
            <Button className="btn btn-round btn-default btn-edit"
              onClick={() => checkIfIsAuthor(userAuth, _setAuthorForm, history)}>
              <i className="fa fa-pencil" /> Criar um Artigo
            </Button>
          }

        </div>
        <Row>
          {(posts) ? posts.map((item, i) => {
            
            return (
              <Col lg="4" md="6" sm="6" xs="12" key={i}>
                <CardPostImageVertical
                  // className="card-vertical-mini"
                  author={users.find(user => user.uid === item.author)}
                  canEdit={item.author === userAuth.uid}
                  category={item.category}
                  description={item.description}
                  id={item.id}
                  image={item.image}
                  imageAlt={item.imageAlt}
                  kind="/post"
                  title={item.title}
                  readTime={item.read}
                  readPost={item.readers && item.readers.users && !!item.readers.users[userAuth.uid]}
                  slug={item.slug}
                  subtitle={item.subtitle}
                  userAuth={userAuth}
                />
              </Col>
            )
          }) : null}
        </Row>
      </Container>
    </div>
  )
}

export default compose( withRouter, AuthorModalConsumer )(Posts);
