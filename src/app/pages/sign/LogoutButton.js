import React, { useState } from 'react'
import FirebaseAuth from "../../firebase/FirebaseAuth";
import { DropdownItem } from 'reactstrap';
import { Redirect } from "react-router-dom";

const LogoutButton = () => {

  const [loggedOut, setLoggedOut] = useState(false);

  const logout = (e) => {
    FirebaseAuth.signOut().then(item => {
      setLoggedOut(true);
    });
  }

  if (loggedOut) {
    return <Redirect to="/presentation" />
  }

  return (
    <DropdownItem
      className="d-flex-center"
      onClick={logout}>
      <i className="fa fa-sign-out mr-3" />
      <span>
        Sair
      </span> 
    </DropdownItem>
  );
  
}
export default LogoutButton;