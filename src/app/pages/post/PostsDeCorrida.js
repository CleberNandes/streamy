import React from 'react';

import Posts from './Posts';
import WhiteNavbar from '../../../components/Navbars/WhiteNavbar';
import FooterBlack from '../../../components/Footers/FooterBlack';
import navbar from '../../../data/NavbarData';
import { AuthenticationConsumer } from '../auth/AuthenticationContext';

const PostsDeCorrida = ({match, userAuth}) => {

  const { category } = match.params;
  
  React.useEffect(() => {
    document.documentElement.classList.remove("nav-open");
    document.body.classList.add("section-page");
    
    return () => {
      document.body.classList.remove("section-page");
    }
  });

  return (
    <>
      <WhiteNavbar navbar={navbar}/>
      <div className="section-space" />
      {/* <SectionHeader /> */}
      <Posts category={category} userAuth={userAuth}/>
      <FooterBlack />
    </>
  );
}

export default AuthenticationConsumer(PostsDeCorrida);