import React, { useState, createContext } from 'react';
import { Modal } from 'reactstrap';
import LoginForm from './LoginForm';
export let ModalLoginContext = createContext(null);

export const ModalLoginProvider = Component => {
  const Provider = (props) => {
    const [_login, _setLogin] = useState(false);
    return (
      <ModalLoginContext.Provider value={_setLogin}>
        <Component {...{...props}} />
        <Modal
          isOpen={_login}
          toggle={() => _setLogin(false)}
          className="modal-register">

          <div className="modal-body">
            <button
              className="close"
              type="button"
              onClick={() => _setLogin(false)}>
              <span>×</span>
            </button>
            <LoginForm setLogin={_setLogin}/>
          </div>
        </Modal>
      </ModalLoginContext.Provider>
    );
    
  }
  return Provider;
};

export const ModalLoginConsumer = Component => {
  const Consumer = (props) => {
    return (
      <ModalLoginContext.Consumer >
        { (_setLogin) => 
          <Component {...{...props, _setLogin}} />
        }
      </ModalLoginContext.Consumer>
    );
  }
  return Consumer;
};