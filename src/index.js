/** 
 * @author Cleber Antônio Fernandes
 * @email cleber_sim@outlook.com
*/

import React from "react";
import ReactDOM from "react-dom";
import App from "./app/App";

ReactDOM.render( <App/>, document.getElementById("root"));
