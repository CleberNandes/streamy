/** 
 * @author Cleber Antônio Fernandes
 * @email cleber_sim@outlook.com
 * @howToUse useInput('', ({target: {value}}) => field.set(value)),
 * @props 
 * @bind object useInput
 * @label string 
 * @required boolean
 * @name string
 * @options Array<string> 
 * @value string
*/

import React from 'react';
import { Label, Input, Row, Col } from 'reactstrap';

function InputRadio({label, options, field, name}) {

  return (
    <div className="form-group">
      <Label>
        {label} 
        {/* { required ? <span className="icon-danger">*</span> : null} */}
      </Label>

      { options ? 
      
        <Row>
          {options.map(item => 
            <Col md="6" sm="6" key={options.indexOf(item)}>
              <div className="form-check-radio">
                <Label check>
                  <Input
                    checked={field.value === item ? true : false}
                    onChange={field.bind.onChange}
                    value={item}
                    name={name}
                    type="radio"/>
                  {item} <span className="form-check-sign" />
                </Label>
              </div>
            </Col>
          )}
        </Row>
      : null }

      { field.error ? 
        <small className="invalid-feedback">
          {field.error}
        </small> 
      : null }

    </div>
  )
}

export default InputRadio;