import React, { useState } from 'react'

// reactstrap components
import { Button, Card, CardTitle, Form} from "reactstrap";
import useInput from '../../../components/form/useInput';
import FormHelper from '../../../helpers/FormHelper';
import FirebaseAuth from '../../firebase/FirebaseAuth';
import InputText from '../../../components/form/InputText';
import ResetPasswordEmailSent from './ResetPasswordEmailSent';

function ResetPasswordForm({setLoginFormActive}) {
  
  const [errorEmail, setErrorEmail] = useState(null);
  const [emailSent, setEmailSent] = useState(false);

  const form = {
    email: useInput('', 'email')
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    const data = FormHelper.getDataFromForm(form);
    FirebaseAuth.passwordReset(data.email)
      .then(() => {
        FormHelper.resetForm(form);
        setEmailSent(true);
      }).catch(error => {
        console.log(error);
        if (error.code === 'auth/user-not-found') {
          setErrorEmail('Nenhum usuário encontrado com esse email!')
        }
      });    
  }

  if (emailSent) {
    return <ResetPasswordEmailSent {...{setEmailSent, setLoginFormActive}}/>
  }

  return (
    <Card className="card-register card-login">
      <CardTitle tag="h3">Welcome</CardTitle>

      <Form className="register-form" onSubmit={handleSubmit}>

        <InputText
          className="no-border"
          placeholder="digite seu email..."
          label="Email"
          errorMsg={errorEmail}
          bind={form.email.bind}
          required={true}
          showOk={false}/>

        <Button block 
          className="btn-round"
          type="submit"
          color="danger">
          Resetar senha
        </Button>
      </Form>

      <div className="forgot">
        
        <Button
          className="btn-link"
          color="danger"
          onClick={() => setLoginFormActive(true)}>
            Faça o login.
        </Button>
      </div>

    </Card>
  )
}
export default ResetPasswordForm;