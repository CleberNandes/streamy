import React from 'react'
import { Card } from 'reactstrap';
import LoginForm from './LoginForm';

function LoginCard({history, setLoginFormActive}) {
  
  return (
    <Card className="card-register card-login">

      <LoginForm {...{history, setLoginFormActive}} />

    </Card>
  )
}
export default LoginCard;