import React from "react";
import DefaultNavbar from './components/DefaultNavbar';

function DangerNavbar({navbar}) {

  navbar.color = {
    change: false,
    color: 'danger'
  }

  return <DefaultNavbar {...{navbar}}/>;
}

export default DangerNavbar;
