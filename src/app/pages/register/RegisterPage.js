import React from "react";
// reactstrap components
import {
  Container,
  Row,
  Col
} from "reactstrap";
// core components

// data 
import navbar from '../../../data/NavbarData';
import RegisterForm from "./RegisterForm";
import ColorNavbar from "../../../components/Navbars/ColorNavbar";

function RegisterPage(props) {
  document.documentElement.classList.remove("nav-open");
  React.useEffect(() => {
    document.body.classList.add("register-page");
    document.body.classList.add("full-screen");
    window.scrollTo(0, 0);
    document.body.scrollTop = 0;
    return function cleanup() {
      document.body.classList.remove("register-page");
      document.body.classList.remove("full-screen");
    };
  });
  return (
    <>
      <ColorNavbar navbar={navbar} />
      <div className="wrapper">
        <div
          className="page-header"
          style={{
            backgroundImage:
              "url(" + require("../../../assets/img/sections/soroush-karimi.jpg") + ")"
          }}
        >
          <div className="filter" />
          <Container>
            <Row>
              <Col className="ml-auto" lg="5" md="5" sm="5" xs="12">

                <div className="info info-horizontal">
                  <div className="icon">
                    <i className="fa fa-umbrella" />
                  </div>
                  <div className="description">
                    <h3>Intereção com artigos</h3>
                    <p>
                      Quando realiza um cadastro vc pode interagir com um artigo, 
                      curtindo, comentando, salvando na sua lista para ler mais tarde 
                      e até os artigos que vc já leu.
                    </p>
                  </div>
                </div>

                <div className="info info-horizontal">
                  <div className="icon">
                    <i className="fa fa-map-signs" />
                  </div>
                  <div className="description">
                    <h3>Seja um colunista</h3>
                    <p>
                      Fazendo nosso cadastro vc pode se tornar um 
                      colunista do blog, postando conteúdo que aca interessante.
                      Seu artigo será revisado pelo nosso time e publicado.
                    </p>
                  </div>
                </div>

                <div className="info info-horizontal">
                  <div className="icon">
                    <i className="fa fa-user-secret" />
                  </div>
                  <div className="description">
                    <h3>Se mantenha atualizado</h3>
                    <p>
                      A área do esporte está em constante evolução. 
                      Tenha acesso a conteúdo atualizado do informações.
                    </p>
                  </div>
                </div>
              </Col>

              <Col className="mt-auto mb-auto" lg="7" md="7" sm="7" xs="12">
                
                <RegisterForm 
                  history={props.history}/>

              </Col>

            </Row>
          </Container>
          <div className="demo-footer text-center">
            <h6>
              © {new Date().getFullYear()}, made with{" "}
              <i className="fa fa-heart heart" /> by Creative Tim
            </h6>
          </div>
        </div>
      </div>
    </>
  );
}

export default RegisterPage;
