
import React, { useState, useEffect } from 'react'
import { Row, Container, Col } from 'reactstrap'
import FirebaseService from '../../app/firebase/Firebase.service';
import CardBackImageDark from '../cards/CardBackImageDark';


export default function RelatedPosts({slug, userAuth}) {

  const [relatedPost, setRelatedPost] = useState(null);
  const [_authors, setAuthors] = useState([]);

  const getRelateds = () => {
    const isInList = relatedPost ? 
      relatedPost.find(item => item.slug === slug) : null;
    if (!relatedPost || isInList) {
      FirebaseService.relatedsLimit(
        'posts', 'created_at', slug, 3, setRelatedPost, userAuth);
    }
  }

  const getUsers = () => {
    if (!_authors.length) {
      FirebaseService.getListFromDatabase('users', 'created_at', setAuthors);
    }
  }

  useEffect(() => {
    getRelateds();
    getUsers();
  })
  
  return (
    <Row>
      <Col md="12">
        <div className="related-articles">
          <h3 className="title">Artigos relacionados</h3>
          <legend />
          <Container>
            <Row>
              {relatedPost && _authors ? 
              relatedPost.map((item, i) => 
                <Col md="4" key={i}>
                  <CardBackImageDark
                    image={item.image}
                    category={item.category}
                    slug={item.slug}
                    title={item.title}
                    description={item.description}
                    author={_authors.find(user => user.uid === item.author)}
                    kind={'/post'}/>
                </Col>
              )
              : null}
            </Row>
          </Container>
        </div>
      </Col>
    </Row>
  )
}