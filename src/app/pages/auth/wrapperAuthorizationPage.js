import React, { useEffect } from 'react';
import FirebaseConfig from '../../firebase/FirebaseConfig';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';
/**
 * Esse componente serve para esconder rotas redirecionando para outra pagina
 */
const wrapperAuthorizationPage = condition => Component => {

  const WithAuthorization = (props) => {

    useEffect(() => {

      const listener = FirebaseConfig.auth.onAuthStateChanged(
        currentUser => {
          if (!condition(currentUser)) {
            props.history.push('/login');
          }
        },
      );

      return () => {
        listener();
      }
    });

    return <Component {...props} />;
  }

  return compose( withRouter )(WithAuthorization);
};

export default wrapperAuthorizationPage;