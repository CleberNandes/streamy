import React from "react";
import DefaultNavbar from './components/DefaultNavbar';

function InfoNavbar(props) {
  props.navbar.color = {
    change: false,
    color: 'info'
  }

  return <DefaultNavbar navbar={props.navbar}/>;
}

export default InfoNavbar;
