
import React, { useEffect, forwardRef, useImperativeHandle } from "react";
import { Button, Row, Col } from "reactstrap";

import InputText from '../../../components/form/InputText';
import useInput from "../../../components/form/useInput";
import ImageUpload from "../../../components/CustomUpload/ImageUpload";
import InputCheckbox from "../../../components/form/InputCheckBox";
import CategoryService from "../../services/Category.service";
import ImageService from "../../services/Image.service";
import GeneralHelpers from "../../../helpers/GeneralHelpers";
import InputRadio from "../../../components/form/InputRadio";
import TagsInputGroup from "../../../components/form/TagsInputGroup";

const FormPost = forwardRef((props, ref) => {
  
  const categories = CategoryService.all();
  const formats = ["leve", "médio", "forte", "iron"];
  const obj = {...props};
  
  if(!obj.post) {
    obj.post = {
      imageAlt : 'acessória de corrida feminina',
    };
  }

  const form = {
    title: useInput(obj.post.title, 'text'),
    subtitle: useInput(obj.post.subtitle, 'textarea'),
    image: useInput(obj.post.image),
    imageAlt: useInput( obj.post.imageAlt, 'text'),
    description: useInput(obj.post.description, 'textarea'),
    priority: useInput(obj.post.priority, 'checkbox'),
    tags: useInput(obj.post.tags, 'tags'),
    format: useInput(obj.post.format, 'text'),
    category: useInput(obj.post.category, 'text'),
    date: useInput(obj.post.date),
    id: useInput(obj.post.id),
    author: useInput(obj.post.author),
    slug: useInput(obj.post.slug)
  }

  const resetForm = () => {
    for (let item in form) {
      form[item].reset();
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (form.image.val && typeof form.image.val !== 'string') {
      ImageService.save('posts', form.image.val, props.save);
    } else {
      props.save(form.image.val);
    }
  };

  const prepareData = (imgUrl) => {
    const data = {};
    for (let item in form) {
      if (form[item].val) {
        data[item] = form[item].val;      
      }
    }
    if (imgUrl) {
      data.image = imgUrl;
    }
    data.tags = data.tags ? data.tags : [];
    data.read = GeneralHelpers.readTime(data.description);
    data.category = CategoryService.getCategory(data.category);
    return data;
  }

  useImperativeHandle(ref, () => ({
    prepareData: (img) => prepareData(img),
    resetForm: () => resetForm(),
  }));

  let fileInput = React.useRef();
  useEffect(() => {
    if (fileInput.current) {
      form.image.reset = fileInput.current.handleRemove;
    }    
  });

  return (
    <form onSubmit={handleSubmit}>
      <Row>
        <Col md="6" sm="6">

          <InputText
            placeholder="digite o título do matéria"
            label="Título"
            bind={form.title.bind}
            required={true}
            showOk={false}/>

          <InputText
            placeholder="digite o sub-título do matéria"
            label="Sub-Título"
            type="textarea"
            rows="3"
            maxLength="400"
            bind={form.subtitle.bind}
            required={true}
            showOk={false}/>

          <InputText
            label="Descrição Longa"
            type="textarea"
            rows="14"
            maxLength="15000"
            placeholder="Aqui vai a conteúdo do texto, separe os paragrafos por enters mantendo uma linha em branco entre eles"
            required={true}
            bind={form.description.bind}/>

          <InputCheckbox
            label="Exibir como prioridade"
            required={false}
            bind={form.priority.bind}/>

          <InputRadio
            label="Categoria"
            options={categories}
            name='categories'
            value={form.category.val}
            bind={form.category.bind}
            required={true}/>

        </Col>

        <Col md="6" sm="6">

          <ImageUpload
            label="Imagem da Capa"
            avatar={false}
            setImage={form.image.set}
            image={form.image.val}
            ref={fileInput}/>

          <InputText
            placeholder="uma breve descrição da imagem"
            label="Descrição da imagem"
            bind={form.imageAlt.bind}
            required={false}
            showOk={false}/>

          <TagsInputGroup
            label="Tags" 
            color="badge-success"
            bind={form.tags.bind}
            unique={true}/>

          <InputRadio
            label="Formato"
            options={formats}
            name='formats'
            value={form.format.val}
            bind={form.format.bind}
            required={true}/>

        </Col>
      </Row>

      <div>
        <Button
          block
          className="btn-round"
          color="primary"
          outline
          type="submit">
          Save
        </Button>
      </div>
    </form>
  );
});

export default FormPost;