const sectionSumaryData = [
  {
    iconColor: 'icon icon-danger',
    icon: 'nc-icon nc-layout-11',
    title: 'Huge Number of Components',
    description: 'The kit comes with components designed to look perfect together. All components fit perfectly with each other.'
  },
  {
    iconColor: 'icon icon-danger',
    icon: 'nc-icon nc-tile-56',
    title: 'Multi-Purpose Sections',
    description: 'The sections will help you generate pages in no time. From team presentation to pricing options, you can easily customise and built your examples.'
  },
  {
    iconColor: 'icon icon-danger',
    icon: 'nc-icon nc-paper',
    title: 'Example Pages',
    description: 'Speed up your development time or get inspired with the large number of example pages. You can jump start your development with our pre-built example pages.'
  }
];

export default sectionSumaryData;