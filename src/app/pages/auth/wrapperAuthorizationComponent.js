import React, { useEffect, useState } from 'react';
import FirebaseConfig from '../../firebase/FirebaseConfig';
/**
 * Esse componente é para esconder componentes caso não esteje logado
 */
const wrapperAuthorizationComponent = condition => Component => {

  const WithAuthorization = (props) => {

    const [allowed, setAllowed] = useState(true);

    useEffect(() => {

      const listener = FirebaseConfig.auth.onAuthStateChanged(
        currentUser => {
          if (!condition(currentUser)) {
            setAllowed(false);
          }
        },
      );

      return () => {
        listener();
      }
    })

    if (!allowed) {
      return null;
    }

    return <Component {...props} />;
  }

  return WithAuthorization;
};

export default wrapperAuthorizationComponent;