import React from 'react';
import { Button, Card, CardTitle } from "reactstrap";

function ResetPasswordEmailSent({setEmailSent, setLoginFormActive}) {

  const handleClick = () => {
    setEmailSent(false);
    setLoginFormActive(true);
  }

  return (
    <Card className="card-register card-login">
      <CardTitle tag="h3">Welcome</CardTitle>

      <div className="forgot">

        <Button
          className="btn-link"
          color="danger"
          onClick={handleClick}>
            Faça login
        </Button>

      </div>

    </Card>
  )
}
export default ResetPasswordEmailSent;