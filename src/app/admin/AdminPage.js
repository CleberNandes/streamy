import React, { useEffect, useState } from 'react';
import FirebaseService from '../firebase/Firebase.service';
import { objInArray } from '../../helpers/GeneralHelpers';
import UserList from './UserList';

function AdminPage() {

  const [_loading, _setLoading] = useState(false);
  const [_users, _setUsers] = useState([]);

  const getUsers = () => {

    _setLoading(true);
    FirebaseService.users().on('value', snapshot => {      
      if (snapshot.val() && objInArray(snapshot.val()).length !== _users.length ) {
        const users = objInArray(snapshot.val());
        _setUsers(users);
        _setLoading(false);
      }
    });
  }

  useEffect(() => {
    getUsers();
    return () => FirebaseService.users().off();
  })

  return (
    <div>
      <h1>AdminPage</h1>
      <UserList users={_users}/>
      <p>loading {_loading.toString()}</p>
    </div>
  )
}

export default AdminPage;