import FirebaseService from "../firebase/Firebase.service";

const save = (collection, image, callback) => {
  FirebaseService.saveImageStorage(collection, image, callback);
};

const remove = (collection, image) => {
  return FirebaseService.deleteImageStorage(collection, image);
};

export default {
  save,
  remove
};