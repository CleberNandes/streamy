import React from 'react';
import { Media } from 'reactstrap';
import { Link } from 'react-router-dom';

export default function PostAuthorInfo({author}) {
  console.log(author);
  

  return (
    <Media className="post-author-info">
      <Link
        className="pull-left"
        to={'/autor/' + author.uid}>
        <div className="avatar big-avatar">
          <Media
            alt={author.name}
            object
            src={author.avatar.url}
          />
        </div>
      </Link>
      <Media body>
        <Media heading>{author.firstName + ' ' + author.lastName}</Media>
        <div className="pull-right">
          <Link
            className="btn btn-round btn-outline-danger"
            to={'/autor/' + author.uid}>
            <i className="fa fa-reply mr-1" />
            Seguir
          </Link>
        </div>
        <p>
          {author.description}
        </p>
      </Media>
    </Media>
  )
}
