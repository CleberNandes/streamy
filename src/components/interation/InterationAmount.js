import React from 'react';
import PropTypes from 'prop-types';
import ReadersCountIcon from './ReadersCountIcon';
import LikersCountIcon from './LikersCountIcon';
import SaversCountIcon from './SaversCountIcon';
import CommentersCountIcon from './CommentersCountIcon';

const InterationAmount = ({user, slug, likers, commenters, savers, readers}) => {

  return (
    <div className="interation-amount">
      
      <LikersCountIcon 
        likers={likers}
        user={user}
        slug={slug}/>

      <CommentersCountIcon
        commenters={commenters}
        user={user}/>

      <SaversCountIcon
        savers={savers}
        user={user}
        slug={slug}/>

      <ReadersCountIcon 
        readers={readers}
        user={user}
        slug={slug}/>

    </div>
  )
}
// const condition = authUser => !!authUser;
// export default wrapperAuthorizationComponent(condition)(InterationAmount);
export default InterationAmount;

InterationAmount.propTypes = {
 likes: PropTypes.number,
 comments: PropTypes.number,
 saved: PropTypes.number,
 read: PropTypes.number,

}
