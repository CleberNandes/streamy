import React from 'react';
import { Route, Redirect } from "react-router-dom";

function Auth({ Component, authUser, path, redirect }) {
  // const counter = useSelector(state => state);
  console.log(authUser, path, redirect);
  
  return (
    
    <Route
      {...{path}}
      render={props =>
        authUser ? 
          <Component {...{...props, authUser}} /> 
          :
          <Redirect to="/login" />
      }
    />
  );
}
export default Auth;