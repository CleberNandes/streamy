import React, { useState } from 'react'
import { Card, CardTitle, Button, Form, Row } from 'reactstrap'
import { Link } from 'react-router-dom';
import useInput from '../../../components/form/useInput';
import FormHelper from '../../../helpers/FormHelper';
import DateHelper from '../../../helpers/DateHelper';
import InputText from '../../../components/form/InputText';
import FirebaseAuth from '../../firebase/FirebaseAuth';
import FirebaseService from '../../firebase/Firebase.service';
import Loader from '../../../components/Loader';
import GoogleAuthBtn from '../../GoogleAuthBtn';

export default function RegisterForm(props) {

  const [_loading, _setLoading] = useState(false);
  const password = useInput('', 'password', {required: '', password: ''});
  const form = {
    name: useInput('', 'text', {required: '', minLength: 4}),
    email: useInput('', 'email', {required: '', email: ''}),
    password,
    confirm: useInput('', 'password', {required: '', minLength: 6, confirm: ''}, password)
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    if (FormHelper.hasError(form)) return;
    _setLoading(true);
    const data = FormHelper.getDataFromForm(form);
    let uid;
    data.created_at = DateHelper.generateDate();
    
    FirebaseAuth.createUser(data.email, data.password)
      .then(({user}) => {
        uid = user.uid;
        return createUserRTDB(uid, data);
      })
      .then(() => {
        FormHelper.resetForm(form);
        props.history.push('/autor/' + uid);
      })
      .catch(error => {
        if (error.code === 'auth/email-already-in-use') {
          form.email.setError('O email já é utilizado por outro usuário!')
        }
      })
      .finally(() => _setLoading(false) );    
  }

  const createUserRTDB = (uid, {name, email}) => {
    return FirebaseService
      .user(uid)
      .set({ uid, name, email });
  }

  return (
    <Card className="card-register">

      <Loader loading={_loading} />

      <CardTitle className="text-center" tag="h3">
        Register
      </CardTitle>

      <div className="social">
        <Button className="btn-just-icon mr-1" color="facebook">
          <i className="fa fa-facebook" />
        </Button>

        <GoogleAuthBtn round={true} />

        <Button className="btn-just-icon" color="twitter">
          <i className="fa fa-twitter" />
        </Button>
      </div>

      <div className="division">
        <div className="line l" />
        <span>or</span>
        <div className="line r" />
      </div>

      <Form className="register-form" onSubmit={handleSubmit}>
        <Row>
          <InputText
            className="col-sm-12 col-md-6"
            placeholder="digite seu nome..."
            label="Nome"
            field={form.name}/>

          <InputText
            className="col-sm-12 col-md-6"
            placeholder="digite seu email..."
            label="Email"
            field={form.email}/>

          <InputText
            className="col-sm-12 col-md-6"
            placeholder="escolha sua senha..."
            label="Senha"
            field={form.password}/>

          <InputText
            className="col-sm-12 col-md-6"
            placeholder="confirme sua senha..."
            label="Confirme a senha"
            field={form.confirm}/>
        </Row>

        <Button block 
          type="submit"
          className="btn-round" 
          color="default">
          Register
        </Button>

        <div className="login ">
          <p>
            Já tem uma conta?{"   "}
            <Link to="/login">
              Log in
            </Link>.
          </p>
        </div>

      </Form>
    </Card>
  )
} 