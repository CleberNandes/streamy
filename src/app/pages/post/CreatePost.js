
import React, { useRef, useEffect } from "react";
import { Container } from "reactstrap";


import FormPost from "./FormPost";
import StringHelper from "../../../helpers/StringHelper";
import DateHelper from "../../../helpers/DateHelper";
import FirebaseService from "../../firebase/Firebase.service";
import DangerNavbar from "../../../components/Navbars/DangerNavbar";
import navbar from "../../../data/NavbarData";
import { AuthenticationConsumer } from "../auth/AuthenticationContext";

function Create({history, userAuth}){
  
  const formRef = useRef();

  const createPost = (imgUrl) => {
    const data = formRef.current.prepareData(imgUrl)
    data.author = userAuth.uid;
    data.slug = StringHelper.passSentenceToSlug(data.title);
    data.created_at = DateHelper.generateDate();
    data.id = new Date().getTime();
    FirebaseService.saveDatabase('posts', data.slug, data)
      .then(() => history.push(`/`));
    formRef.current.resetForm();
  };

  useEffect(() => {
    if (!userAuth.author) {
      history.push('/posts')
    }
  })

  return (
    
    <>
      <DangerNavbar navbar={navbar}/>
      <div className="main">
        <div className="section">
          <Container>
            <h1 className="h2 mb-3">Criar um novo post</h1>
            <FormPost
              ref={formRef}
              save={createPost}/>
          </Container>
        </div>
      </div>
    </> 
  );
}

export default AuthenticationConsumer(Create);