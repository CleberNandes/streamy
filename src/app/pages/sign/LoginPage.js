import React, { useState } from "react";
// reactstrap components
import { Container, Row, Col } from "reactstrap";
import ColorNavbar from "../../../components/Navbars/ColorNavbar";
import navbar from "../../../data/NavbarData";
import ResetPasswordForm from "./ResetPasswordForm";
import LoginCard from "./LoginCard";
// data 

function LoginPage({history}) {
  document.documentElement.classList.remove("nav-open");

  const [loginFormActive, setLoginFormActive] = useState(true);

  React.useEffect(() => {
    document.body.classList.add("login-page");
    document.body.classList.add("full-screen");
    window.scrollTo(0, 0);
    document.body.scrollTop = 0;
    return function cleanup() {
      document.body.classList.remove("login-page");
      document.body.classList.remove("full-screen");
    };
  });
  return (
    <>
      <ColorNavbar navbar={navbar} />
      <div className="wrapper">
        <div
          className="page-header"
          style={{
            backgroundImage:
              "url(" + require("../../../assets/img/sections/bruno-abatti.jpg") + ")"
          }}>
          <div className="filter" />
          <Container>
            <Row>
              <Col className="ml-auto mr-auto" lg="4" md="6" sm="6">

                { loginFormActive ?
                  <LoginCard {...{history, setLoginFormActive}} />
                  :
                  <ResetPasswordForm {...{setLoginFormActive}}/>
                }

              </Col>
            </Row>

            <div className="demo-footer text-center">
              <h6>
                © {new Date().getFullYear()}, made with{" "}
                <i className="fa fa-heart heart" /> by Creative Tim
              </h6>
            </div>

          </Container>
        </div>
      </div>
    </>
  );
}

export default LoginPage;
